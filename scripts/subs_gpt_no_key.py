import requests
import json
import argparse
import os

"""
Following is the script for enhancing the existing subtitles with Chat GPT
"""

GPT_MODEL = 'gpt-4-1106-preview' # use 'gpt-4' or 'gpt-3.5-turbo'

def main():
    """Main function"""

    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--Input', help='Input json file name')
    args = parser.parse_args()

    # preparation: read and divide the long text into a list of subtitles
    # file_name = '060/out1.txt'

    if args.Input:
        file_name = args.Input
    else:
        print('Invalid file!')
        raise

    with open(file_name) as file:
        sub_list = file.read().split('\n\n')

    # step 2: divide the list into chunks
    chunked_list = chunk_list(sub_list)

    # step 3: improve the subs
    improved_chunks = improve_subs(chunked_list)

    return '\n\n'.join(improved_chunks)


def chunk_list(subs, chunk_size=100_000):
    """
    Divide subs into chunks for fewer API calls
    """
    chunked = []
    for i in range(0, len(subs), chunk_size): 
        temp = []
        for j in range(0, chunk_size):
            if i + j >= len(subs):
                break
            temp.append(subs[i + j])
        chunked.append('\n\n'.join(temp))
    return chunked


def improve_subs(chunks):
    """
    Call GPT API to impove subtitles
    """
    endpoint = 'https://api.openai.com/v1/chat/completions'
    headers = {
        'Authorization': 'Bearer {}'.format(os.environ.get('GPT_KEY')),
        'Content-Type': 'application/json',
    }

    # read the prompt from a separate file
    prompt_file = 'prompt.txt'
    with open(prompt_file) as text:
        prompt = text.read()

    improved = []

    for instance in chunks:

        data = {
            'model': GPT_MODEL,
            'messages': [
                {"role": "user", "content": prompt + '\n' + instance}
            ]
        }
        try:
            response = requests.post(endpoint, headers=headers, data=json.dumps(data))
            response_json = response.json()
            # print(response_json)
            improved.append(response_json['choices'][0]['message']['content'])
        except:
            print('EXCEPTION RAISED\n', response_json)

    return improved


if __name__ == '__main__':
    print(main())