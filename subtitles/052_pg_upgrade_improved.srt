1
00:00:00,06 --> 00:00:01,0999999
And I'm alone again.

2
00:00:01,5 --> 00:00:03,54
Hello, hello, this is episode number
52.

3
00:00:03,9399998 --> 00:00:06,72
I screwed up a little bit in the
beginning because I forgot to

4
00:00:07,24 --> 00:00:09,3
unmute myself in Zoom.

5
00:00:10,16 --> 00:00:15,08
Thank you, live audience, which
I have on YouTube, who helped

6
00:00:15,08 --> 00:00:16,6
me to unmute myself.

7
00:00:17,1 --> 00:00:19,6
But unfortunately, I didn't notice
it at the beginning.

8
00:00:19,6 --> 00:00:21,18
But okay, let's proceed.

9
00:00:21,18 --> 00:00:26,3
So we have pg_upgrade, which is
a very good tool for an upgrade.

10
00:00:26,72 --> 00:00:31,1
In the ancient times, we needed
to dump restore our database

11
00:00:31,24 --> 00:00:33,28
to perform a major upgrade.

12
00:00:33,82 --> 00:00:35,78
But now we have pg_upgrade, great.

13
00:00:36,28 --> 00:00:40,68
But actually, we discussed it with
Michael some time ago.

14
00:00:40,68 --> 00:00:43,34
We had an episode about minor and
major upgrades.

15
00:00:43,48 --> 00:00:47,96
We discussed high-level problems
that we usually have and so

16
00:00:47,96 --> 00:00:48,28
on.

17
00:00:48,28 --> 00:00:53,14
But let me discuss particular problems
when you use pg_upgrade

18
00:00:53,3 --> 00:00:55,16
to run major upgrades.

19
00:00:56,52 --> 00:01:03,68
So the first problem is, if you have
a huge database and some significant

20
00:01:03,74 --> 00:01:09,26
traffic to it, and you want to
avoid downtime, there is a well-known

21
00:01:09,56 --> 00:01:13,979996
idea: let's use logical to avoid
downtime.

22
00:01:15,06 --> 00:01:18,84
Of course, if you use pg_upgrade
as it is, it will take significant

23
00:01:18,88 --> 00:01:19,38
time.

24
00:01:19,44 --> 00:01:22,26
And there is another recipe described
in the documentation to

25
00:01:22,26 --> 00:01:29,3
use the "-k", or "--link", option,
which significantly speeds

26
00:01:29,3 --> 00:01:32,38
up the process, because we switch
to using hard links.

27
00:01:32,92 --> 00:01:36,48
Most files in pgdata are not
going to change during upgrade,

28
00:01:36,58 --> 00:01:37,98
so hard links help.

29
00:01:39,86 --> 00:01:43,82
But some people say, okay, we don't
want even some minutes of

30
00:01:43,82 --> 00:01:48,04
downtime. It will usually take up to
a few ten or fifteen minutes,

31
00:01:48,04 --> 00:01:51,52
depending on cluster size and additional
steps you perform.

32
00:01:52,58 --> 00:01:57,479996
They say, we want to do it faster,
let's involve logical replication.

33
00:01:57,86 --> 00:02:00,12
And here we have a couple of dangers.

34
00:02:01,16 --> 00:02:05,28
First of all, if the cluster is really
large, it's very challenging

35
00:02:05,28 --> 00:02:07,28
to initialize a logical replica.

36
00:02:08,22 --> 00:02:13,3
Because when you initialize it
in a traditional way, it's fully

37
00:02:13,3 --> 00:02:13,8
automated.

38
00:02:13,98 --> 00:02:20,52
If you just create a replication
subscription and start a subscription

39
00:02:20,6 --> 00:02:26,18
to a new empty cluster, if it's
already a new Postgres, great,

40
00:02:26,18 --> 00:02:29,44
it will be done automatically,
but you will find it very challenging

41
00:02:29,44 --> 00:02:33,58
for really big databases, like
for example, 5, 10, 15 terabytes

42
00:02:33,7 --> 00:02:39,76
under some significant load, like
at least thousands or a few

43
00:02:39,76 --> 00:02:41,92
dozens of thousands of TPS.

44
00:02:42,7 --> 00:02:43,94
And why is it so?

45
00:02:43,94 --> 00:02:47,96
Because initialization at a logical
level involves basically

46
00:02:47,96 --> 00:02:53,12
a kind of dump-restore process,
which takes long, and also it

47
00:02:53,12 --> 00:02:57,26
affects the health of the source
cluster, which is currently in use, right?

48
00:02:57,26 --> 00:02:59,48
your main cluster under use, right?

49
00:03:01,06 --> 00:03:05,74
And before Postgres 16, which is
not yet released, yesterday

50
00:03:05,74 --> 00:03:13,64
we had only beta 2, and also the
branch RL16, stable was stamped,

51
0:03:13,98 --> 0:03:18,06
Meaning that we're already close
to release, but still not there.

52
0:03:18,54 --> 0:03:23,24
And PostgreSQL 16 will allow you
to logically replicate from

53
0:03:23,24 --> 0:03:23,74
standbys.

54
0:03:23,86 --> 0:03:25,12
Right now it's not possible.

55
0:03:25,52 --> 0:03:29,6
So it's possible only from primary,
but even if it's from standby,

56
0:03:30,1 --> 0:03:31,38
the problem will remain.

57
0:03:31,64 --> 0:03:37,54
So you affect how the vacuum workers
work and they cannot remove

58
0:03:38,1 --> 0:03:42,84
freshly dead tuples because you're
still taking data and you

59
0:03:42,84 --> 0:03:44,34
need that snapshot.

60
0:03:44,44 --> 0:03:50,32
So after the moment you started,
if some tuples became dead,

61
0:03:50,38 --> 0:03:53,9
Autovacuum cannot remove those tuples,
and you will see in logs

62
0:03:53,9 --> 0:03:58,02
that some number of tuples are
dead but cannot yet be removed.

63
0:03:58,32 --> 0:03:58,68
Why?

64
0:03:58,68 --> 0:04:2,36
Because we still hold snapshot,
because we're still initializing

65
0:04:2,52 --> 0:04:3,22
our replica.

66
0:04:3,58 --> 0:04:7,9
And sometimes it's even not possible
to finish.

67
0:04:8,32 --> 0:04:11,84
Like, on the primary you produce
a lot of load, so it's kind

68
0:04:11,84 --> 0:04:13,16
of difficult.

69
0:04:13,66 --> 0:04:15,92
And I actually didn't do it myself,
never.

70
0:04:15,92 --> 0:04:19,84
I just know it feels not the right
approach, but my colleagues did

71
0:04:19,84 --> 0:04:25,52
and they tried and they failed, even
on smaller database, like about

72
0:04:25,52 --> 0:04:26,26
1 terabyte.

73
0:04:27,44 --> 0:04:29,34
So how to do it better?

74
0:04:29,34 --> 0:04:30,18
There's a recipe.

75
0:04:30,18 --> 0:04:31,66
So we call it physical-to-logical.

76
0:04:32,9 --> 0:04:35,94
And it's quite straightforward.

77
0:04:38,3 --> 0:04:41,66
We can first of all have a secondary
cluster.

78
0:04:42,66 --> 0:04:45,3
I'm going to use Patroni terminology
here.

79
0:04:45,36 --> 0:04:48,92
So we have a secondary cluster
with its own standby leader.

80
0:04:49,28 --> 0:04:54,14
They are all standbys, but 1 of
them is leader because we have

81
0:04:54,14 --> 0:04:55,14
cascaded replication.

82
0:04:55,64 --> 0:04:59,98
So standby leader is replicating
using streaming replication from

83
0:05:00,46 --> 0:05:01,6
main primary.

84
0:05:02,08 --> 0:05:05,26
And then it has its own standby
nodes, cascaded replication.

85
0:05:05,98 --> 0:05:09,86
And Patroni can do that, you can
say I'm having a secondary cluster 

86
0:05:09,86 --> 0:05:13,94
here, so it shouldn't have a real
primary, but it has its own

87
0:05:13,94 --> 0:05:14,88
standby leader.

88
0:05:16,24 --> 0:05:20,4
This streaming replication is asynchronous,
normal streaming 

89
0:05:20,44 --> 0:05:20,94
replication.

90
0:05:21,66 --> 0:05:27,1
And then we create a slot and we
create publication on the source,

91
0:05:27,1 --> 0:05:28,54
on the primary, as I said.

92
0:05:28,94 --> 0:05:33,78
And once we do it, WALs are starting
to accumulate on the primary

93
0:05:33,78 --> 0:05:34,44
more and more.

94
0:05:34,44 --> 0:05:39,92
If we keep this slot unused, we
know we will be out of disk space 

95
0:05:39,92 --> 0:05:42,5
on the main primary, which we are
using right now.

96
0:05:42,5 --> 0:05:46,42
So we should move fast, or we should
have a lot of disk space.

97
0:05:46,64 --> 0:05:49,78
And of course, it's not good to
spend a lot of time accumulating

98
0:05:50,08 --> 0:05:51,98
this lag basically, right?

99
0:05:52,38 --> 0:05:55,58
If no one is using this slot, we
have LSN position.

100
0:05:55,76 --> 0:05:59,7
We know LSN position when we create
a slot using SQL approach.

Subtitles:
101
00:06:00,04 --> 00:06:03,4
You can do it in two ways, but SQL
approach is fine as well here.

102
00:06:03,4 --> 00:06:06,42
So you say, select pg_create_replication
slot, it will return

103
00:06:06,42 --> 00:06:07,08
you LSN.

104
00:06:07,96 --> 00:06:10,94
So the slot is at this position.

105
00:06:11,18 --> 00:06:11,68
Good.

106
00:06:12,34 --> 00:06:13,64
Now we have a trick.

107
00:06:13,66 --> 00:06:18,62
We, on our future primary, which
is currently a standby leader.

108
00:06:19,02 --> 00:06:21,0
We just adjust configuration.

109
00:06:21,42 --> 00:06:27,88
We say, instead of replicating
everything using physical replication,

110
00:06:29,18 --> 00:06:33,34
we have a setting which is called
recovery_target_lsn.

111
00:06:34,02 --> 00:06:38,94
And we put this LSN from the slot
position, we put it right there,

112
00:06:38,94 --> 00:06:41,96
restart server, and let it catch
up and pause.

113
00:06:42,26 --> 00:06:45,26
There is another setting, I don't
remember the name, which says

114
00:06:45,92 --> 00:06:51,18
which action to perform when something
like this is achieved.

115
00:06:51,18 --> 00:06:53,86
Like, okay, we achieved this LSN
and we pause.

116
00:06:54,96 --> 00:06:55,46
Great.

117
00:06:55,46 --> 00:07:00,46
So, and not replicating at physical
level anything additional.

118
00:07:00,82 --> 00:07:05,24
By the way, we found that if you
use WAL shipping here, sometimes

119
00:07:05,6 --> 00:07:08,54
it replicates slightly more than
needed.

120
00:07:09,18 --> 00:07:11,1
So we need to explore this probably.

121
00:07:11,12 --> 00:07:15,14
But if you use streaming replication,
this works really good.

122
00:07:15,8 --> 00:07:21,14
So at this point, we can shut down.

123
00:07:22,28 --> 00:07:23,4
And here we have a trick.

124
00:07:23,4 --> 00:07:25,24
Here we have a problem, number
2.

125
00:07:25,24 --> 00:07:29,9
So first of all, this already allows
us to switch from physical

126
00:07:29,9 --> 00:07:31,04
replication to logical.

127
00:07:31,84 --> 00:07:36,36
And this is how we can very quickly
initialize logical replica

128
00:07:36,82 --> 00:07:41,88
based on physical replica in cases
when you need all of database

129
00:07:41,88 --> 00:07:44,04
or majority of tables to be replicated.

130
00:07:44,04 --> 00:07:49,24
Of course, with logical you can
replicate only some tables, and

131
00:07:49,24 --> 00:07:52,54
this recipe is not good in this
case, if you need only, for example,

132
00:07:52,54 --> 00:07:54,3
10% of all your tables.

133
00:07:54,62 --> 00:08:01,66
But if you need everything, and
in the case of a major upgrade,

134
00:08:01,96 --> 00:08:02,94
we need everything.

135
00:08:03,16 --> 00:08:06,88
In this case, this physical to
logical recipe is really good.

136
00:08:08,18 --> 00:08:10,61
But we have now a second problem
here.

137
00:08:10,61 --> 00:08:12,68
The first problem we already solved,
right?

138
00:08:12,98 --> 00:08:18,42
Instead of regular insertion, we
use physical replicants, which

139
00:08:18,42 --> 00:08:19,64
are physical to logical.

140
00:08:19,64 --> 00:08:26,54
By the way, when we create a slot,
when we create a subscription,

141
00:08:27,88 --> 00:08:31,36
we need to say explicitly that
we already have everything.

142
00:08:31,4 --> 00:08:32,62
So we just need CDC.

143
00:08:32,72 --> 00:08:34,52
Logical consists of 2 stages.

144
00:08:34,7 --> 00:08:35,76
First, 2 steps.

145
00:08:35,76 --> 00:08:41,06
First is full initialization, so
bring snapshot of data.

146
00:08:41,26 --> 00:08:45,92
And second is CDC, change data
capture, so replaying changes

147
00:08:45,92 --> 00:08:46,62
from slot.

148
00:08:47,04 --> 00:08:50,54
So in case of physical to logical
conversion, we don't need the

149
00:08:50,54 --> 00:08:53,3
first one because physical already
brought us everything.

150
00:08:53,56 --> 00:08:56,52
And how we initialized replica,
physical replica doesn't matter.

151
0:08:56,52 --> 0:09:2,06
It could be a pg_basebackup, it
could be pgBackRest or WAL-G fetching

152
0:09:02,62 --> 0:09:04,20
the whole database from backups.

153
0:09:04,60 --> 0:09:07,66
It could be cloud snapshots or
anything else.

154
0:09:08,44 --> 0:09:10,94
So now we have a second problem.

155
0:09:11,72 --> 0:09:13,52
This problem is quite interesting.

156
0:09:16,54 --> 0:09:17,66
It feels like this.

157
0:09:17,74 --> 0:09:24,14
If you first initialize logical
replication and then shut down

158
0:09:24,78 --> 0:09:29,66
your logical replica, run pg_upgrade
on it and start again, there

159
0:09:29,66 --> 0:09:31,18
are risks of data corruption.

160
0:09:31,72 --> 0:09:38,76
This is quite interesting and it
was discussed in Hacker News mailing

161
0:09:38,76 --> 0:09:41,42
list a few months ago.

162
0:09:41,42 --> 0:09:44,80
There is a discussion, I'm going
to provide a link in the description.

163
0:09:46,60 --> 0:09:48,54
So it's quite kind of interesting.

164
0:09:48,66 --> 0:09:54,50
So when we run pg_upgrade, it doesn't
play well with logical replication.

165
0:09:55,08 --> 0:10:00,20
Instead of this, let's just swap
the order of our actions.

166
0:10:00,44 --> 0:10:05,86
We know that our future primary
is at the position of the slot.

167
0:10:06,06 --> 0:10:09,90
But instead of starting to use
logical replication right away,

168
0:10:10,24 --> 0:10:13,40
let's postpone it and first let's
run pg_upgrade.

169
0:10:14,24 --> 0:10:21,02
We run pg_upgrade, we start our
new cluster, which has all data.

170
0:10:21,26 --> 0:10:25,06
And we know it was frozen at the
position we need.

171
0:10:25,38 --> 0:10:27,26
It's synchronized with the slot,
right?

172
0:10:27,44 --> 0:10:31,56
So we started, it has timeline
1 or 2, so it's kind of like a

173
0:10:31,56 --> 0:10:32,46
fresh database.

174
0:10:33,64 --> 0:10:38,50
And then we can, after it, we can
start using logical replication,

175
0:10:38,68 --> 0:10:43,86
create subscription, and of course,
during the pg_upgrade, the

176
0:10:43,86 --> 0:10:47,86
slot will accumulate some lag,
because the LSN position is frozen,

177
0:10:48,26 --> 0:10:49,20
it's behind.

178
0:10:49,34 --> 0:10:54,84
So of course, pg_upgrade should
be done as fast as possible, for

179
0:10:54,84 --> 0:10:57,84
example, involving hardlinks, the
option "-k".

180
0:10:58,70 --> 0:11:00,56
In this case, it will take a few
minutes.

181
0:11:03,48 --> 0:11:08,00
We know that in the new cluster after
pg_upgrade we don't have statistics,

182
0:11:08,04 --> 0:11:09,44
so we need to run analyze.

183
0:11:10,52 --> 0:11:14,56
I would say it's not a good idea
to run analyze right away.

184
0:11:15,06 --> 0:11:20,10
We can start logical replication first and
run analyze logical subscription

185
0:11:20,16 --> 0:11:24,40
first, start this CDC process,
change data capture process.

186
0:11:25,20 --> 0:11:29,62
And in parallel to run analyze
using many workers, for example,

187
0:11:29,62 --> 0:11:34,90
vacuumdb, dash dash analyze, dash
J, some, maybe even number

188
0:11:34,90 --> 0:11:37,62
of process, cores we have.

189
0:11:37,96 --> 0:11:43,26
And if disks are very good, SSD
and so on, it's, it should be

190
0:11:43,26 --> 0:11:43,76
fine.

191
0:11:44,34 --> 0:11:49,24
And during that we already, the
logical replication already working,

192
0:11:49,64 --> 0:11:50,94
it's catching up.

193
0:11:51,22 --> 0:11:55,22
We don't have this risk of data
corruption discussed in the Hacker News

194
0:11:55,24 --> 0:11:56,10
mailing list.

195
0:11:57,38 --> 0:12:02,74
And for logical, Of course, all
tables should have primary key

196
0:12:02,78 --> 0:12:05,82
or replica identity full.

197
0:12:06,58 --> 0:12:09,84
If we have primary key, we don't
actually need statistics to

198
0:12:09,84 --> 0:12:10,76
work with primary key.

199
0:12:10,76 --> 0:12:15,92
So all inserts, updates, deletes
will work even before we collect

200
0:12:15,92 --> 0:12:16,42
statistics.

201
0:12:16,44 --> 0:12:20,14
So we can run an analyze operation in parallel
or slightly later and so on,

202
0:12:20,14 --> 0:12:21,1
it will be fine.

203
0:12:21,22 --> 0:12:27,14
We just shouldn't run regular user
queries on this node yet,

204
0:12:27,34 --> 0:12:30,28
until we collect statistics.

205
0:12:30,72 --> 0:12:32,8
So in this case, this is a good
recipe.

206
0:12:33,58 --> 0:12:38,24
But there is another problem, and
this problem is not only related

207
0:12:38,24 --> 0:12:40,68
to this recipe involving logical.

208
0:12:41,52 --> 0:12:44,76
By the way, I forgot to mention
that a couple of weeks ago a

209
0:12:44,76 --> 0:12:50,78
new tool was published, which is
written in Ruby, and it allows

210
0:12:50,86 --> 0:12:56,6
to automate the process involving
logical and run PostgreSQL major

211
0:12:56,6 --> 0:12:57,94
upgrade using logical.

212
0:12:58,22 --> 0:13:1,74
It was discussed on Hacker News,
It was discussed on Twitter,

213
0:13:1,86 --> 0:13:8,32
and at least 5 people sent me this
tool because they know I'm

214
0:13:8,32 --> 0:13:10,08
interested in PostgreSQL upgrades.

215
0:13:10,58 --> 0:13:11,4
Thank you all.

216
0:13:15,26 --> 0:13:18,12
But I think This tool is okay,
I think.

217
0:13:18,82 --> 0:13:22,76
I briefly checked it and I didn't
see this physical to logical

218
0:13:22,82 --> 0:13:23,32
conversion.

219
0:13:23,36 --> 0:13:26,82
So it means that if you have a
really big database, probably

220
0:13:26,82 --> 0:13:28,38
this tool won't work well.

221
0:13:28,78 --> 0:13:36,34
And second, I'm not sure about the order
of actions, so you should

222
0:13:36,34 --> 0:13:37,0
be careful.

223
0:13:37,2 --> 0:13:40,48
Because if the order of actions is logical
first, then upgrade and continue

224
0:13:40,48 --> 0:13:42,54
logical, there are risks of corruption.

225
0:13:43,14 --> 0:13:47,16
So I would encourage you to check
this, explore this, and communicate

226
0:13:47,16 --> 0:13:49,34
with the author, maybe I will do it
as well.

227
0:13:49,94 --> 0:13:53,2
It's kind of very tricky parts
and dangerous parts.

228
0:13:53,24 --> 0:13:56,74
That's why I call this episode
PgUpgrade Tricky and Dangerous

229
0:13:56,8 --> 0:13:57,3
Parts.

230
0:13:58,1 --> 0:14:3,54
So, finally, Let's discuss the
final, maybe the most dangerous

231
0:14:3,54 --> 0:14:4,04
part.

232
0:14:4,22 --> 0:14:6,14
Because it's not only about logical.

233
0:14:6,88 --> 0:14:9,82
It's about anyone who uses pg_upgrade.

234
0:14:11,12 --> 0:14:16,76
Even, like, okay, most of people
use with hard links, with option-k

235
0:14:17,12 --> 0:14:18,22
right now, or dash-link.

236
0:14:19,74 --> 0:14:21,96
But it's not even about that.

237
0:14:22,92 --> 0:14:26,52
The problem is related to how we
upgrade standby nodes.

238
0:14:27,7 --> 0:14:33,0
And Postgres documentation describes
the recipe involving rsync.

239
0:14:33,62 --> 0:14:41,04
It says, okay, once you upgraded
the primary, to upgrade the

240
0:14:41,04 --> 0:14:45,04
standbys, which already have some
data, just run rsync to eliminate

241
0:14:45,06 --> 0:14:49,08
deviation we accumulated during
running PgUpgrade.

242
0:14:49,28 --> 0:14:50,02
And that's it, right?

243
0:14:50,02 --> 0:14:51,0
It's quite obvious.

244
0:14:51,68 --> 0:14:56,26
But the problem is, if you really
want to minimize downtime,

245
0:14:56,82 --> 0:15:1,28
in case of without logical, in
downtime, usually a few minutes,

246
0:15:1,28 --> 0:15:4,9
we want to achieve a few minutes
even with clusters that are huge, many

247
0:15:4,9 --> 0:15:7,74
many many terabytes, we want just
a few minutes of downtime.

248
0:15:8,42 --> 0:15:14,92
If you want to minimize it, you
cannot use rsync in best mode.

249
0:15:14,92 --> 0:15:17,86
The best mode for rsync would be
checksum.

250
0:15:18,24 --> 0:15:23,7
So rsync checksum would check the
content of all files.

251
0:15:24,06 --> 0:15:25,98
But it would take a lot.

252
0:15:26,4 --> 0:15:31,78
Maybe it's easier to recreate a
replica than just wait until

253
0:15:31,78 --> 0:15:33,46
rsync's checksum is working.

254
0:15:33,52 --> 0:15:36,3
Unfortunately, rsync is a single-threaded
thing.

255
0:15:36,6 --> 0:15:40,74
So if you want to benefit from
the fact that you have fast disks

256
0:15:40,74 --> 0:15:46,58
and a lot of cores, you need to
parallelize it somehow yourself.

257
0:15:47,14 --> 0:15:49,76
Maybe with GNU parallel or something
like that.

258
0:15:49,76 --> 0:15:51,76
So it's like kind of a task.

259
0:15:52,2 --> 0:15:54,06
And documentation doesn't describe
it.

260
0:15:54,92 --> 0:16:1,5
But if you just follow the documentation,
it says rsync.

261
0:16:1,84 --> 0:16:2,34
Okay.

262
0:16:2,44 --> 0:16:3,38
There's another option.

263
0:16:3,38 --> 0:16:6,52
So if you don't specify checksum,
you just use rsync.

264
0:16:6,9 --> 0:16:11,2
It will compare 2 things, size
of the file and modification time.

265
0:16:11,2 --> 0:16:15,72
It's much better, but still it's
a question to me, will it be

266
0:16:15,72 --> 0:16:16,22
reliable?

267
0:16:17,02 --> 0:16:21,74
But the least reliable approach
is to use the option size only.

268
0:16:21,88 --> 0:16:23,9
So we will compare only size.

269
0:16:24,64 --> 0:16:29,6
And in this case, guess what documentation
describes?

270
0:16:29,6 --> 0:16:31,16
The documentation describes size only.

271
0:16:32,04 --> 0:16:39,88
And imagine some table, which is
quite big, say 100 gigabytes.

272
0:16:40,8 --> 0:16:47,22
Postgres stores tables and indexes
using files, 1-gigabyte files,

273
0:16:47,46 --> 0:16:49,16
many, many 1-gigabyte files.

274
0:16:49,16 --> 0:16:53,98
You can find them using the OID of
the table or index, and you can see

275
0:16:54,02 --> 0:16:58,4
OID.1, OID.2, so many files, all
of them are 1 gigabyte.

276
0:16:59,18 --> 0:17:5,28
So if some writes happened during
your upgrade, which you are

277
0:17:5,28 --> 0:17:11,54
not propagated to standbys, and
then you follow the official Postgres

278
0:17:12,04 --> 0:17:16,1
recipe, which is
currently official, and just

279
0:17:16,1 --> 0:17:18,5
run rsync size only, as it describes.

280
0:17:19,46 --> 0:17:23,84
Rsync size only, we'll see, okay,
we have blah blah blah dot

281
0:17:23,84 --> 0:17:24,64
to the file.

282
0:17:25,16 --> 0:17:28,12
1 gigabyte on the source, 1 gigabyte
on the target, no difference.

283
0:17:31,56 --> 0:17:39,24
So, this is super interesting,
because I suspect many of the

284
0:17:39,24 --> 0:17:44,02
clusters upgraded recently, or
maybe not that recently, have

285
0:17:44,02 --> 0:17:44,94
corrupted standbys.

286
0:17:47,22 --> 0:17:51,84
And we had it also with some quite
big database last week.

287
0:17:52,12 --> 0:17:56,04
We upgraded using our logical recipe,
this kind of magic and

288
0:17:56,04 --> 0:17:56,69
so on.

289
0:17:56,69 --> 0:18:4,62
And fortunately, we saw corruption
on replicas, on new replicas,

290
0:18:5,02 --> 0:18:9,44
when we moved read-only traffic,
user traffic, there, and we

291
0:18:9,44 --> 0:18:13,14
started seeing corruption errors
in logs.

292
0:18:13,14 --> 0:18:16,64
Corruption errors are very important
to monitor, and we have

293
0:18:16,64 --> 0:18:19,64
3 error codes.

294
0:18:19,64 --> 0:18:21,38
Postgres has 3 error codes.

295
0:18:21,38 --> 0:18:25,4
You should always monitor and have
alerts on each.

296
0:18:27,72 --> 0:18:31,72
It's xx001, xx002.

297
0:18:32,48 --> 0:18:35,46
These are at the very bottom of the
error code page.

298
0:18:35,46 --> 0:18:37,32
In the documentation, you can find
this.

299
0:18:37,54 --> 0:18:39,0
And the last three lines.

300
0:18:39,24 --> 0:18:41,5
This is the most...

301
0:18:45,06 --> 0:18:47,3
You should have alerts on this,
definitely.

302
0:18:48,1 --> 0:18:51,98
But a funny thing during major
operations, such as a major upgrade,

303
0:18:51,98 --> 0:18:54,36
you probably silence all alerts.

304
0:18:54,52 --> 0:18:59,12
So, you need to have manual checks
of logs, as well.

305
0:18:59,6 --> 0:19:1,42
And this is how we found out.

306
0:19:2,8 --> 0:19:6,94
Accidentally we were checking logs
and saw a couple of lines

307
0:19:6,94 --> 0:19:9,64
and we saw this, wow, it looks
like corruption.

308
0:19:9,64 --> 0:19:14,98
And then we found out that all
standbys—we had maybe five—

309
0:19:16,56 --> 0:19:21,06
for some files, 1 gigabyte files,
have slightly different content

310
0:19:21,06 --> 0:19:23,0
than the future primary.

311
0:19:23,16 --> 0:19:24,22
We rolled back.

312
0:19:24,64 --> 0:19:28,84
So, fortunately, no data loss, nothing
like that happened.

313
0:19:29,06 --> 0:19:34,02
So we had only a few errors, like
thirty maybe errors or so for user

314
0:19:34,02 --> 0:19:36,1
traffic and that's it, which is
great.

315
0:19:36,6 --> 0:19:40,24
But then I started to think, this
is the official recipe.

316
0:19:41,04 --> 0:19:44,8
And the documentation, this recipe,
how to perform upgrades,

317
0:19:44,8 --> 0:19:49,02
it mentions that you should check
with pg_controldata, you should

318
0:19:49,02 --> 0:19:56,36
check the last checkpoint position
when you stop standby and

319
0:19:56,48 --> 0:20:0,54
primary on the future cluster,
new cluster.

320
0:20:1,12 --> 0:20:7,84
But it says something like, they
might have differences if, for

321
0:20:7,84 --> 0:20:12,62
example, you stopped standbys first
or if standbys are still running,

322
0:20:12,62 --> 0:20:13,72
something like that.

323
0:20:13,78 --> 0:20:17,58
But it doesn't say that you shouldn't
proceed if you see a difference,

324
0:20:17,64 --> 0:20:18,5
first of all.

325
0:20:18,74 --> 0:20:19,94
And it doesn't mention...

326
0:20:21,2 --> 0:20:25,46
Right now, the connection is very,
very uncertain about the order

327
0:20:25,46 --> 0:20:29,76
of how we should stop servers and
how we should proceed with

328
0:20:29,76 --> 0:20:33,12
this dangerous rsync size only.

329
0:20:33,66 --> 0:20:37,06
So that's why I'm thinking many
clusters were upgraded like that.

330
0:20:37,06 --> 0:20:40,44
And if you don't have monitoring
of corruption, probably you

331
0:20:40,44 --> 0:20:42,66
still have corruption on standby
nodes.

332
0:20:42,72 --> 0:20:44,1
It sounds very dangerous.

333
0:20:44,88 --> 0:20:49,12
I wrote to hackers yesterday, to
the mailing list, and Robert

334
0:20:49,12 --> 0:20:53,68
has already admitted that this
recipe looks not right, it should

335
0:20:53,68 --> 0:20:54,34
be improved.

336
0:20:54,88 --> 0:20:57,94
But, let's discuss how to do it
better.

337
0:20:59,14 --> 0:21:3,56
Thinking how to do it better, talking
to guys and to Andrei Borodin,

338
0:21:3,56 --> 0:21:12,16
as well, checking some interesting
slides.

339
0:21:12,8 --> 0:21:17,8
Standby leader, our new primary,
which was already upgraded before

340
0:21:17,8 --> 0:21:18,24
the upgrade.

341
0:21:18,24 --> 0:21:24,36
If we keep it running, and when
we keep running also all standbys,

342
0:21:24,38 --> 0:21:30,24
and then we shut down primary,
but don't shut down all its standbys,

343
0:21:30,58 --> 0:21:34,3
Meaning that we need to disable
autofailover, such as Patroni,

344
0:21:34,9 --> 0:21:36,46
to put it in maintenance mode.

345
0:21:36,46 --> 0:21:39,86
We shouldn't allow autofailover
to happen in this case.

346
0:21:40,12 --> 0:21:43,62
But the idea is we keep standbys
running and allow them to fully

347
0:21:43,62 --> 0:21:45,14
catch up all lags.

348
0:21:46,02 --> 0:21:50,78
When you shut down some server,
primary, a walsender has some

349
0:21:50,78 --> 0:21:56,28
logic to make an attempt to write
everything to standbys, to

350
0:21:56,28 --> 0:21:58,54
send all changes before shutting
down.

351
0:21:58,86 --> 0:22:0,84
And then we also can involve pg_controldata.

352
0:22:1,56 --> 0:22:3,9
So the idea is let's keep standbys
running.

353
0:22:4,34 --> 0:22:7,5
We check also with pg_controldata
checkpoint position, which

354
0:22:7,5 --> 0:22:8,14
is good.

355
0:22:8,8 --> 0:22:13,78
Then we upgrade primary, and then
probably we still keep running

356
0:22:13,78 --> 0:22:14,28
standbys.

357
0:22:14,5 --> 0:22:19,04
And then probably 1 by 1, we shut
1 down, do rsync, or probably

358
0:22:19,04 --> 0:22:23,9
we shut all of them down and rsync
in parallel with all nodes.

359
0:22:24,58 --> 0:22:30,74
And this recipe actually I saw
in Yandex.Mail presentation, and

360
0:22:30,74 --> 0:22:35,3899
I learned yesterday that this recipe
with some work was done

361
0:22:35,3899 --> 0:22:35,865
from...

362
0:22:35,865 --> 0:22:40,7
Some work was coming from the guys
from Yandex Mail in the past,

363
0:22:40,76 --> 0:22:45,18
related to the use of hard links
and vSAR snippet and so on.

364
0:22:45,18 --> 0:22:51,42
But I think it's super important
to ensure that standbys received

365
0:22:51,42 --> 0:22:56,18
all changes from primary, and if
they didn't, probably you will

366
0:22:56,18 --> 0:22:56,82
have corruption.

367
0:22:57,28 --> 0:22:58,4
Actually that's it.

368
0:22:58,86 --> 0:23:1,6
I hope it was interesting, dangerous
enough.

369
0:23:2,46 --> 0:23:3,54
Dangerous I mean...

370
0:23:5,34 --> 0:23:10,22
The lesson here is definitely to
have monitoring and alerting

371
0:23:10,28 --> 0:23:16,82
for corruption errors, XXO01, XXO01
or XXOO2, these 3 errors

372
0:23:17,78 --> 0:23:20,78
probably involve some additional
checks.

373
0:23:21,0 --> 0:23:27,62
For example, using pg_amcheck, my
colleagues learned that pg_amcheck

374
0:23:27,62 --> 0:23:33,48
also found corruption on replicas
if you run it, because there

375
0:23:33,48 --> 0:23:37,86
was a deviation between files in
index and files in data.

376
0:23:38,76 --> 0:23:44,14
And then also, when you run pg_upgrade,
the safest, I think, is

377
0:23:44,14 --> 0:23:45,66
just to initialize the replicas.

378
0:23:45,66 --> 0:23:49,46
If you can afford it, just re-initialize
it from scratch, avoiding

379
0:23:49,64 --> 0:23:50,14
this.

380
0:23:50,5 --> 0:23:58,48
But if you cannot afford it, at
least be very careful with this

381
0:23:59,2 --> 0:24:2,54
risk of deviation.

382
0:24:3,1 --> 0:24:10,96
Or maybe run rsync without data
size only option, because this

383
0:24:10,96 --> 0:24:12,78
size only option definitely doesn't...

384
0:24:13,08 --> 0:24:17,86
I cannot guarantee that without
this option, with default behavior

385
0:24:17,86 --> 0:24:21,42
of rsync, when it checks size and
modification time, you will

386
0:24:21,42 --> 0:24:21,88
be fine.

387
0:24:21,88 --> 0:24:23,14
I cannot say yet.

388
0:24:23,36 --> 0:24:25,32
So I think some work should be
done.

389
0:24:25,32 --> 0:24:29,32
And if you have ideas, please join
the discussion on the hackers

390
0:24:29,64 --> 0:24:31,6
mailing list, which I started yesterday.

391
0:24:32,28 --> 0:24:37,04
So yeah, let me check a couple
of questions I have in chat because

392
0:24:37,04 --> 0:24:38,18
it's a live session.

393
0:24:39,92 --> 0:24:43,26
Are we expecting to have modifications
in official documentation

394
0:24:43,26 --> 0:24:45,64
to ensure safer upgrades in upcoming
releases?

395
0:24:45,9 --> 0:24:47,62
Well, Postgres is open source.

396
0:24:47,62 --> 0:24:49,3
You can propose your own modification.

397
0:24:49,64 --> 0:24:53,3
I'm still thinking I have questions
here, more than answers.

398
0:24:53,4 --> 0:24:56,54
Once I have some answers, I'm definitely
going to propose if I

399
0:24:56,54 --> 0:25:2,48
have some idea which
how to improve, but I guess the

400
0:25:2,48 --> 0:25:5,58
documentation should be changed
for all Postgres versions.

401
0:25:6,98 --> 0:25:7,88
So, okay.

402
0:25:8,04 --> 0:25:9,12
Thank you so much.

403
0:25:9,24 --> 0:25:14,88
It was some strange episodes again,
and next week I hope Michael

404
0:25:15,06 --> 0:25:18,58
is returning and we will have anniversary
1 year for our podcast.

405
0:25:18,84 --> 0:25:19,78
Thank you so much.

406
0:25:19,78 --> 0:25:24,24
Subscribe, share in your social
networks and working groups where

407
0:25:24,24 --> 0:25:27,26
you work with your colleagues who
are also interested in Postgres,

408
0:25:27,7 --> 0:25:31,78
and send us comments with your
feedback and ideas for the future.

409
0:25:31,98 --> 0:25:32,52
Thank you.