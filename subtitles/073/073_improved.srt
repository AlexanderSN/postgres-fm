1
00:00:00,06 --> 00:00:02,44
Michael: Hello and welcome to PostgresFM, a weekly show about

2
00:00:02,44 --> 00:00:03,36
all things PostgreSQL.

3
00:00:03,64 --> 00:00:05,26
I am Michael, founder of pgMustard.

4
00:00:05,38 --> 00:00:08,20
This is my co-host, Nikolay Samokhvalov, founder
of Postgres.ai.

5
00:00:08,48 --> 00:00:10,88
Hey, Nikolay, what are we talking
about today?

6
00:00:11,58 --> 00:00:12,34
Nikolay: Hi, Michael.

7
00:00:12,66 --> 00:00:16,56
Let's talk about sub-transactions
and be very short with this.

8
00:00:16,56 --> 00:00:21,04
This is a super simple topic because
I have a super simple rule,

9
00:00:21,04 --> 00:00:24,50
just don't use sub-transactions
unless you're absolutely necessary.

10
00:00:24,96 --> 00:00:31,30
And I'm going to describe details
but not going too deep.

11
00:00:31,34 --> 00:00:33,06
Let's keep this episode short.

12
00:00:34,28 --> 00:00:35,10
Michael: Sounds great.

13
00:00:35,24 --> 00:00:38,68
And also it's Thanksgiving so hopefully
people are spending a

14
00:00:41,48 --> 00:00:42,56
lot of time with their families.

15
00:00:42,56 --> 00:00:46,68
So happy Thanksgiving to anybody
that celebrates and hopefully

16
00:00:46,68 --> 00:00:48,46
you'll be thankful for a short
episode.

17
00:00:49,44 --> 00:00:49,94
Nikolay: Right.

18
00:00:50,08 --> 00:00:53,80
I wanted to thank you, of course,
for doing this with me so long.

19
00:00:54,52 --> 00:00:55,02
Likewise.

20
00:00:56,94 --> 00:00:57,84
Michael: It's been a pleasure.

21
00:00:59,54 --> 00:01:00,88
Where should we start then?

22
00:01:01,86 --> 00:01:05,80
Nikolay: So, let's start with the
idea of sub-transactions very

23
00:01:05,80 --> 00:01:06,30
briefly.

24
00:01:06,58 --> 00:01:13,92
The idea is if you have a complex
transaction and you want to handle

25
00:01:14,12 --> 00:01:19,80
some kind of failure, like for
example, a statement might fail,

26
00:01:20,98 --> 00:01:26,08
but you don't want to lose the
previous statements already completed

27
00:01:26,14 --> 00:01:32,94
in the same transaction or you
want, for example, to explicitly

28
00:01:34,32 --> 00:01:39,00
undo some steps, right? It might
be multiple statements you

29
00:01:39,00 --> 00:01:42,62
can return to a previous point.

30
00:01:43,18 --> 00:01:47,72
So you can use sub-transactions,
also called nested transactions,

31
00:01:48,14 --> 00:01:51,54
in some cases, although it's not
very nested, but kind of, okay,

32
00:01:51,54 --> 00:01:52,28
it's nested.

33
00:01:52,36 --> 00:01:58,14
Nested transactions, also there
is an SQL standard keyword I think

34
00:01:58,14 --> 00:02:01,70
it's SQL standard, right? Savepoint.

35
00:02:01,70 --> 00:02:04,30
Michael: Yeah, I don't know if it's
standard, but I think it is

36
00:02:04,30 --> 00:02:07,90
because I read the Postgres docs
for this and they mention one

37
00:02:07,90 --> 00:02:10,22
tiny part of it that's not standard
compliant.

38
00:02:10,60 --> 00:02:12,78
And so therefore I assume everything
else is?

39
00:02:13,48 --> 00:02:15,26
Nikolay: Right, yeah, I think it's
standard.

40
00:02:15,30 --> 00:02:16,56
What was that part?

41
00:02:16,56 --> 00:02:18,72
I also remember, but I don't remember
which part.

42
00:02:18,72 --> 00:02:21,66
Michael: Oh, it gets into the details,
but it's around the naming

43
00:02:21,66 --> 00:02:22,32
of sub-transactions.

44
00:02:22,60 --> 00:02:25,90
So you can name them so that you
can then roll back to the name.

45
00:02:25,90 --> 00:02:29,44
And it's if you use the same name
multiple times, the standard

46
00:02:29,44 --> 00:02:33,26
says you should discard previous
ones.

47
00:02:33,26 --> 00:02:37,44
Whereas the Postgres implementation
allows you to roll back to

48
00:02:37,44 --> 00:02:40,90
one with the same name and then roll
back to the same name again,

49
00:02:40,90 --> 00:02:42,74
but that rolls back to the previous
one.

50
00:02:42,74 --> 00:02:47,06
Anyway, is it, hopefully, that made some sense.

51
0:2:47,64 --> 0:2:49,28
Nikolay: So the idea is nice, right?

52
0:2:49,28 --> 0:2:54,16
If you have really complex transactions
and you expect that you

53
0:2:54,16 --> 0:2:57,9
need to roll back sometimes, you
just put some savepoints in

54
0:2:57,9 --> 0:3:1,02
various places and you can return
to them.

55
0:3:1,02 --> 0:3:4,94
And you can imagine it like, there
are 2 ways to imagine it.

56
0:3:5,22 --> 0:3:6,82
I'm not sure both are correct.

57
0:3:6,82 --> 0:3:8,24
Let's discuss it.

58
0:3:8,24 --> 0:3:13,04
So 1 is like we have some flat
picture and savepoints and we

59
0:3:13,04 --> 0:3:13,82
can jump.

60
0:3:13,94 --> 0:3:15,26
And another is nested.

61
0:3:15,36 --> 0:3:18,48
And I think nested is more correct.

62
0:3:19,4 --> 0:3:24,46
Although if you look at code, usually
it doesn't have an indentation,

63
0:3:24,74 --> 0:3:25,24
right?

64
0:3:25,58 --> 0:3:32,04
It's like flat, vertical, same
indented lines of code, save 0.1,

65
0:3:32,04 --> 0:3:36,26
savepoint name 1, savepoint name
2, and some statements between

66
0:3:36,26 --> 0:3:36,76
them.

67
0:3:36,76 --> 0:3:41,26
And you can rollback to 1 of previous
savepoints.

68
0:3:41,4 --> 0:3:47,78
But internally, there is nesting
there applied, because when

69
0:3:47,78 --> 0:3:53,56
you return to some older savepoint
you lose the deeper, like

70
0:3:53,56 --> 0:3:57,44
newer, fresher savepoints because
they are internally implemented

71
0:3:57,44 --> 0:3:58,54
like kind of deeper.

72
0:3:58,68 --> 0:4:5,04
And 1 of the problems we will discuss
is related to this level

73
0:4:5,34 --> 0:4:9,82
of nesting or depth of this nesting,
right?

74
0:4:10,12 --> 0:4:13,28
Michael: Yes, and I think just
on the basics, I think it's worth

75
0:4:13,28 --> 0:4:17,52
mentioning that even if you know
that you haven't used savepoints

76
0:4:17,52 --> 0:4:20,04
anywhere in your code, you could
still be using sub-transactions

77
0:4:20,74 --> 0:4:21,24
Nikolay: without...

78
0:4:21,98 --> 0:4:22,48
Exactly.

79
0:4:23,0 --> 0:4:25,52
Yeah, or it can call it nested
transactions.

80
0:4:25,52 --> 0:4:30,8
So I think nesting is the right
concept here.

81
0:4:30,9 --> 0:4:33,74
Although, again, it looks like
not nested, but it's nested.

82
0:4:33,82 --> 0:4:43,68
And I think Django, they use the nesting
word, and maybe some other

83
0:4:43,82 --> 0:4:48,72
ORMs will provoke you into using this,
to use it, like implicitly.

84
0:4:49,02 --> 0:4:52,2
So you just do something and you
don't know, but you use it.

85
0:4:52,2 --> 0:4:56,64
Also, PL/pgSQL will provoke you to
use it because if you, at some

86
0:4:56,64 --> 0:5:0,92
point if you use a lot of PL/pgSQL
code as I do, at some point

87
0:5:0,92 --> 0:5:6,46
you will want to have BEGIN ... EXCEPTION
blocks, EXCEPTION when blah

88
0:5:6,46 --> 0:5:7,12
blah blah.

89
0:5:7,36 --> 0:5:10,28
So just to handle some failures.

90
0:5:10,68 --> 0:5:15,34
And this is 100% already subtransactional
because there is another

91
0:5:15,34 --> 0:5:20,58
BEGIN block which defines the main
body of the PL/pgSQL function.

92
0:5:21,18 --> 0:5:21,82
Right, so

93
0:5:21,82 --> 0:5:23,16
Michael: that's the big one I think.

94
0:5:23,16 --> 0:5:26,62
So the ORMs can be using it behind
the scenes without you realizing

95
0:5:26,78 --> 0:5:30,28
and if you've got any exception
handling in functions, whether

96
0:5:30,28 --> 0:5:35,92
that's functions or triggers, like
you are using subtransactions

97
0:5:36,1 --> 0:5:37,54
or you might be using subtransactions.

98
0:5:38,86 --> 0:5:39,36
Nikolay: Right.

99
0:5:39,62 --> 0:5:43,34
Any exception handling or maybe
the top-level exception handling

100
0:5:43,38 --> 0:5:43,88
at...

101
0:5:44,16 --> 0:5:46,6
Is it possible, like, say, begin
blah blah?

102
0:5:47,36 --> 0:5:50,88
We always have begin and block
for top level, right?

103
0:5:50,98 --> 0:5:51,68
Michael: Like implicitly?

104
0:5:52,5 --> 0:5:53,98
Oh no, you mean in a function?

105
0:5:54,54 --> 0:5:55,92
Nikolay: I think

106
0:5:55,92 --> 0:5:58,5
Michael: Exception, I think as
soon as you're using the exception

107
0:5:58,5 --> 0:5:59,94
clause, you are using...

108
0:6:0,92 --> 0:6:3,24
Nikolay: Yeah, I think so as well,
but I'm just curious.

109
0:6:3,24 --> 0:6:7,06
Okay, but definitely if you use
additional begin, exception,

110
0:6:7,2 --> 0:6:11,74
end blocks inside the body of the
PL/pgSQL function, it's already

111
0:6:11,74 --> 0:6:13,44
a sub-transaction, for sure.

112
0:6:13,44 --> 0:6:15,04
So it's an implicit sub-transaction.

113
0:6:15,48 --> 0:6:18,06
So what are the problems with them?

114
0:6:18,74 --> 0:6:21,3
Let's be short as we promised.

115
0:6:22,12 --> 0:6:29,16
The very first problem is that
we have still 4-byte transaction

116
0:6:29,18 --> 0:6:29,68
IDs.

117
0:6:31,22 --> 0:6:38,8
4-byte transaction ID means that
we have 4.2 billion space and

118
0:6:38,8 --> 0:6:44,44
we can use only half of it because
one half of it is the past,

119
0:6:44,44 --> 0:6:45,96
one half of it is the future.

120
0:6:46,32 --> 0:6:50,06
So there is a transaction ID wraparound
problem which happens

121
0:6:50,06 --> 0:6:52,58
after 2.1 billion transactions.

122
0:6:53,36 --> 0:6:56,2
If freezing didn't happen, if you
blocked autovacuum, for example,

123
0:6:56,2 --> 0:7:0,64
with long transaction or something,
or just turned it off, but

124
0:7:0,64 --> 0:7:4,02
it should wake up even if you turn
it on or off.

125
0:7:4,2 --> 0:7:12,48
Okay, so we have 4 byte transaction
IDs, and to optimize how

126
0:7:12,48 --> 0:7:16,8
these IDs are spent, there is also
the concept of virtual transaction

127
0:7:16,8 --> 0:7:17,08
ID.

128
0:7:17,08 --> 0:7:22,7
So if you just run select or some
read-only transaction, it won't

129
0:7:23,26 --> 0:7:26,12
increment the transaction ID counter,
right?

130
0:7:26,12 --> 0:7:29,72
So you won't waste XID value.

131
0:7:29,72 --> 0:7:36,76
And if you use sub-transactions,
a transaction with a couple

132
0:7:36,76 --> 0:7:39,14
of sub-transactions is already
three XIDs.

133
0:7:39,14 --> 0:7:40,74
Michael: Yeah, that's the big issue,
right?

134
0:7:40,76 --> 0:7:42,88
Well, not the big issue, but in
this context.

135
0:7:43,18 --> 0:7:44,08
In this context.

136
0:7:45,6 --> 0:7:47,07715
Nikolay: It's not a big issue,
but you move faster.

137
0:7:47,07715 --> 0:7:50,78
You start spending these quite,
like, not cheap XIDs faster

138
0:7:51,42 --> 0:7:55,42
than if you didn't use subtransactions.

139
0:7:56,2 --> 0:8:0,72
But also there are MXIDs,
so it's a different topic, right?

140
0:8:0,72 --> 0:8:3,7
So this is not, I think it's not
a super big topic, it's just

141
0:8:3,7 --> 0:8:7,78
worth understanding that we move
faster if you use subtransactions

142
0:8:8,2 --> 0:8:8,98
quite often.

143
0:8:9,14 --> 0:8:11,14
Michael: Cool, let's move on to
subnet A.

144
0:8:11,46 --> 0:8:16,22
Nikolay: Yeah, second problem is
this nesting level, like depth

145
0:8:16,4 --> 0:8:17,22
of nesting.

146
0:8:17,54 --> 0:8:22,9
There is a cache and if you reach
nesting level 64, I don't know

147
0:8:22,9 --> 0:8:28,18
who does it, but this is quite
well documented in mailing list

148
0:8:28,18 --> 0:8:31,94
and other places, quite well documented
overflow problem.

149
0:8:32,66 --> 0:8:38,04
Michael: Yeah, so we're talking
about doing 64 levels of that

150
0:8:38,04 --> 0:8:39,1
nesting depth.

151
0:08:39,38 --> 0:08:43,1
I did see a blog post, was it by,
it was on the Cybertec blog,

152
0:08:43,1 --> 0:08:44,2
I think, by Lawrence?

153
0:08:44,24 --> 0:08:44,7
Was that on

154
0:08:44,7 --> 0:08:44,98
Nikolay: this?

155
0:08:44,98 --> 0:08:45,26
Yeah.

156
0:08:45,26 --> 0:08:48,18
Michael: Yeah, so I think it's
quite easy to demonstrate this

157
0:08:48,18 --> 0:08:49,18
problem in...

158
0:08:49,18 --> 0:08:51,92
Nikolay: Right, and we had benchmark
for it, synthetic benchmark,

159
0:08:51,94 --> 0:08:54,6
we did it, it was easy.

160
0:08:54,6 --> 0:09:01,78
So you just, too many savepoints,
and you reach level 65, this

161
0:09:01,78 --> 0:09:04,98
is per-backend cache, so it's
like a local cache, and in this

162
0:09:04,98 --> 0:09:06,58
case, degradation is obvious.

163
0:09:07,08997 --> 0:09:07,58997
So

164
0:09:08,1 --> 0:09:09,64
Michael: it's not a hard limit,
right?

165
0:09:09,64 --> 0:09:12,72
It's just you pay a performance
penalty at that point.

166
0:09:12,72 --> 0:09:14,92
And I've never seen this in the
real world.

167
0:09:14,92 --> 0:09:16,4
Have you ever seen a case of this?

168
0:09:16,4 --> 0:09:17,96
Nikolay: Well, only in synthetic
tests.

169
0:09:17,96 --> 0:09:18,6
Michael: Yeah, okay, fine.

170
0:09:18,6 --> 0:09:19,04
Yeah, great.

171
0:09:19,04 --> 0:09:19,54
Nikolay: Right.

172
0:09:19,54 --> 0:09:21,18
So not in production.

173
0:09:21,38 --> 0:09:27,72
And it's not hard in terms of the
problems it gives, but it's

174
0:09:27,72 --> 0:09:28,26
a hard limit.

175
0:09:28,26 --> 0:09:29,16
You cannot change it.

176
0:09:29,16 --> 0:09:32,86
I mean, it's a constant in the source
code, so it's hard.

177
0:09:33,22 --> 0:09:34,94
But it's not huge.

178
0:09:34,94 --> 0:09:38,08
You can go there, but probably
don't want to go there.

179
0:09:38,08 --> 0:09:41,1
So a third problem is related to
multixacts.

180
0:09:41,48 --> 0:09:46,56
This is additional space of transaction
IDs, multitransaction IDs,

181
0:09:47,4 --> 0:09:48,62
multixact IDs.

182
0:09:49,9 --> 0:09:55,24
Actually, I see it's not very well
documented, these things.

183
0:09:55,24 --> 0:09:58,92
For example, multixact wraparound
also might happen.

184
0:09:59,44 --> 0:10:04,38
We don't see big blog posts like
from Sentry or MailChimp as

185
0:10:04,38 --> 0:10:06,26
for regular transaction IDs.

186
0:10:06,9 --> 0:10:08,8
But it still can happen.

187
0:10:08,8 --> 0:10:13,68
So documentation doesn't have good
words, like details about

188
0:10:13,68 --> 0:10:16,04
how to properly monitor it and
so on.

189
0:10:16,04 --> 0:10:17,46
But I mean, it's possible.

190
0:10:17,56 --> 0:10:23,62
Anyway, multixact, it's a mechanism,
it's like a different space

191
0:10:23,62 --> 0:10:28,98
and it's a mechanism when multiple
sessions establish a lock

192
0:10:28,98 --> 0:10:30,12
on the same object.

193
0:10:31,1 --> 0:10:34,84
For example, like a share lock for
example.

194
0:10:35,14 --> 0:10:38,82
For example, when you have foreign
keys, you deal with multixact

195
0:10:39,72 --> 0:10:40,9
implicitly again.

196
0:10:40,9 --> 0:10:45,9
So PostgreSQL establishes multiple
locks on the same object and

197
0:10:47,32 --> 0:10:49,74
a multixact value is assigned.

198
0:10:50,24 --> 0:10:53,24
And there is a problem, like, there
is a very good blog post

199
0:10:53,24 --> 0:10:57,46
from Nelson LH, how to pronounce?

200
0:10:57,9 --> 0:11:02,36
Michael: I have no idea, but Nelson
seems like a good bet for

201
0:11:2,36 --> 0:11:3,28
The first name.

202
0:11:3,28 --> 0:11:5,58
I'll link it up in the show notes,
of course.

203
0:11:5,6 --> 0:11:8,5
Nikolay: Right, this is an excellent
blog post from 2021.

204
0:11:9,44 --> 0:11:13,18
I'm not sure which company it was,
but the blog post explains

205
0:11:13,78 --> 0:11:15,7
several problems, actually, as
well.

206
0:11:15,72 --> 0:11:18,62
And it explains, like, there is
a well-known problem.

207
0:11:18,64 --> 0:11:21,98
Honestly, I must admit, I didn't
know about that problem as well,

208
0:11:21,98 --> 0:11:24,24
but the blog post assumes everyone
knows it.

209
0:11:24,24 --> 0:11:24,72
Okay.

210
0:11:24,72 --> 0:11:27,66
So the problem, it's not related
to subtransactions, but there

211
0:11:27,66 --> 0:11:29,84
is a problem with SELECT FOR UPDATE.

212
0:11:30,64 --> 0:11:32,82
So there is like a performance cliff.

213
0:11:33,06 --> 0:11:34,82
This blog post explains it.

214
0:11:34,82 --> 0:11:39,4
So once I read it, I started to
be very careful when recommending

215
0:11:39,52 --> 0:11:45,6
to use SELECT FOR UPDATE to parallelize,
for example, queue-like

216
0:11:45,72 --> 0:11:46,22
workloads.

217
0:11:46,86 --> 0:11:47,16
But you

218
0:11:47,16 --> 0:11:48,68
Michael: can SELECT FOR SHARE, 
right?

219
0:11:49,14 --> 0:11:50,1
Nikolay: SELECT FOR UPDATE.

220
0:11:50,2 --> 0:11:50,64
Oh, okay.

221
0:11:50,64 --> 0:11:51,22
This is...

222
0:11:51,22 --> 0:11:52,86
It's not related to subtransactions.

223
0:11:53,2 --> 0:11:54,16
We start from there.

224
0:11:54,16 --> 0:11:54,96
Michael: So it says,

225
0:11:54,96 --> 0:11:58,16
Nikolay: there is a performance
cliff related to SELECT FOR UPDATE.

226
0:11:58,32 --> 0:12:0,28
Okay, it expands details, blah,
blah.

227
0:12:0,28 --> 0:12:0,98
Okay, but...

228
0:12:1,16003 --> 0:12:4,2
And I now understand, okay, SELECT
FOR UPDATE we should be very

229
0:12:4,2 --> 0:12:5,78
careful with it.

230
0:12:6,28 --> 0:12:11,54
So that's why I became a bigger
fan of single session consumption

231
0:12:11,84 --> 0:12:15,86
of queue-like workloads, because
usually with single session

232
0:12:15,86 --> 0:12:17,22
you can move very fast.

233
0:12:17,36 --> 0:12:20,66
Because once you start using SELECT FOR UPDATE,
you can meet this

234
0:12:20,66 --> 0:12:21,3
performance cliff.

235
0:12:21,3 --> 0:12:24,64
Of course, at a larger scale, like
you have many thousands of TPS,

236
0:12:24,64 --> 0:12:25,3
for example.

237
0:12:25,84 --> 0:12:29,48
So, okay, forget about SELECT FOR UPDATE,
then the post explains

238
0:12:29,54 --> 0:12:30,04
SELECT FOR SHARE.

239
0:12:30,82 --> 0:12:31,32
We're...

240
0:12:32,28 --> 0:12:33,28
Oh, sorry.

241
0:12:34,9 --> 0:12:38,82
Sorry, I maybe messed up with this.

242
0:12:39,4 --> 0:12:42,72
You think SELECT FOR SHARE is the
known problem and SELECT FOR

243
0:12:42,72 --> 0:12:48,92
UPDATE is where subtransactions
come to play with, yeah, so

244
0:12:48,92 --> 0:12:52,26
yes, sorry, sorry, sorry, yes.

245
0:12:52,26 --> 0:12:56,0
So, SELECT FOR SHARE is a very
well-known problem as the blog post

246
0:12:56,0 --> 0:12:56,5
explains.

247
0:12:56,98 --> 0:13:0,72
But then if we use SELECT FOR UPDATE
and subtransactions, we

248
0:13:0,72 --> 0:13:2,62
have similar problems as with SELECT FOR SHARE.

249
0:13:3,06 --> 0:13:3,76
I'm very sorry.

250
0:13:3,76 --> 0:13:4,26
Yes.

251
0:13:4.6 --> 0:13:10.46
So if you use SELECT FOR UPDATE, for
example, queue-like workloads

252
0:13:10.48 --> 0:13:14.16
and you have multiple consumers
and you don't want them to fight

253
0:13:14.16 --> 0:13:16.6
with lock acquisition, so you
use SELECT FOR UPDATE.

254
0:13:17.02 --> 0:13:21.3
And also you use subtransactions,
you might hit a similar performance

255
0:13:21.32 --> 0:13:22.66
cliff as for SELECT FOR SHARE.

256
0:13:23.4 --> 0:13:23.84
Right.

257
0:13:23.84 --> 0:13:24.56
Now right.

258
0:13:24.68 --> 0:13:30.74
So anyway, watch out, be very careful
if you use a lot of, like

259
0:13:30.74 --> 0:13:35.5
thousands of TPS, be careful with
both SELECT FOR SHARE and SELECT FOR UPDATE.

260
0:13:36.54 --> 0:13:39.18
This is the explanation.

261
0:13:39.44 --> 0:13:43.98
Again, I'm making mistakes here
because I didn't see it myself

262
0:13:43.98 --> 0:13:45.52
in production.

263
0:13:45.54 --> 0:13:49.12
I saw it in synthetic workloads
again, but not in production.

264
0:13:49.12 --> 0:13:51.3
That's why I already keep on forgetting.

265
0:13:52.34 --> 0:13:55.54
What do you think about this problem
particularly?

266
0:13:55.88 --> 0:13:58.32
Michael: Yeah, you've made me question
which way around it is,

267
0:13:58.32 --> 0:14:2.98
and I'm not even sure if it's worth
rereading this if this is

268
0:14:2.98 --> 0:14:6.88
something that you make extensive
use of at high volume and want

269
0:14:6.88 --> 0:14:7.9
to be aware of.

270
0:14:7.9 --> 0:14:10.76
But if neither of us have seen
it in the real world, especially

271
0:14:10.76 --> 0:14:14.28
you, it feels to me like this is
not the one most likely.

272
0:14:14.34 --> 0:14:17.38
We've done three issues already and
I think it's the fourth that's

273
0:14:17.38 --> 0:14:20.44
going to be most likely to actually
affect people and most interesting.

274
0:14:21.22 --> 0:14:25.2
Nikolay: I'm not sure because again,
this post by Nelson explains

275
0:14:25.2 --> 0:14:29.14
if you use Django for example and
it uses nested transactions you

276
0:14:29.14 --> 0:14:32.52
have savepoints and then you think
oh it's a good idea I have

277
0:14:32.52 --> 0:14:36.38
parallel workers consuming like
fighting with the same rows to

278
0:14:36.38 --> 0:14:39.38
consume queue workloads or lock them.

279
0:14:39.38 --> 0:14:40.9
I'm going to use SELECT FOR UPDATE.

280
0:14:40.9 --> 0:14:44.34
And here you have the similar performance
as they explained for

281
0:14:44.34 --> 0:14:45.82
SELECT FOR SHARE.

282
0:14:47.22 --> 0:14:53.06
But again, it happens only under
very heavy loads, like thousands

283
0:14:53.08 --> 0:14:53.74
of TPS.

284
0:14:53.94 --> 0:14:58.27
Michael: Yeah, and one point they
made in fixing it was they changed...

285
0:14:58.27 --> 0:14:59.16
For Django, even?

286
0:14:59.2 --> 0:15:0.26
Yes, for Django.

287
0:15:1.12 --> 0:15:5.16
There was a parameter they could
change that passed savepoint

288
0:15:5.16 --> 0:15:6.0
equals false.

289
0:15:8.8 --> 0:15:11.32
Which completely fixed the issue,
right?

290
0:15:11.32 --> 0:15:11.82
Like it...

291
0:15:11.82 --> 0:15:13.44
Disabled savepoints, exactly.

292
0:15:13.44 --> 0:15:13.78
Yeah.

293
0:15:13.78 --> 0:15:14.28
Yeah.

294
0:15:14.34 --> 0:15:15.8
It's great that people can do that.

295
0:15:15.8 --> 0:15:18.78
And it's really interesting that
that's not the default.

296
0:15:19.4 --> 0:15:23.92
Nikolay: Again, we fixed problems,
getting rid of subtransactions.

297
0:15:25.68 --> 0:15:27.98
Michael: And actually, before we
move on from that post, they

298
0:15:27.98 --> 0:15:32.14
included one more line that I really
liked, which was them talking

299
0:15:32.14 --> 0:15:36.02
to somebody else who they considered
an expert on the Postgres

300
0:15:36.02 --> 0:15:36.52
side.

301
0:15:37,72 --> 0:15:41,44
And they quoted, sub-transactions
are basically cursed, rip them

302
0:15:41,44 --> 0:15:41,76
out.

303
0:15:41,76 --> 0:15:41,82
—

304
0:15:41,82 --> 0:15:42,32
Nikolay: Cursed.

305
0:15:42,44 --> 0:15:43,54
Michael: — Yeah, cursed.

306
0:15:44,16 --> 0:15:45,06995
— Well, I saw...

307
0:15:45,06995 --> 0:15:45,48
—

308
0:15:45,48 --> 0:15:47,52
Nikolay: No, I'm like the...

309
0:15:49,26 --> 0:15:50,31
How to say?

310
0:15:50,31 --> 0:15:54,04
I'm bringing these thoughts to
people and sometimes I see pushbacks

311
0:15:54,14 --> 0:15:56,98
like, no, no, no, we are still
good.

312
0:15:57,04 --> 0:15:59,86
Well, sorry, it's not only my thought.

313
0:15:59,86 --> 0:16:2,12
I inherited it from this post as
well.

314
0:16:2,12 --> 0:16:5,52
But so let me explain what we saw
at GitLab.

315
0:16:5,64 --> 0:16:7,26
Michael: Yeah, this is problem
number 4.

316
0:16:7,48 --> 0:16:10,98
Nikolay: Problem number 4, and
I saw it myself and helped troubleshoot

317
0:16:11,0 --> 0:16:16,34
and they had also great blog post
about this. And this led to

318
0:16:17,02 --> 0:16:20,62
a big effort to eliminate sub-transactions
in GitLab code.

319
0:16:20,9 --> 0:16:28,54
So there is another cache which
is kind of global, not per session,

320
0:16:28,62 --> 0:16:33,56
and it has also a threshold 16
hard-coded in source code, there

321
0:16:33,56 --> 0:16:34,9
is a constant 16.

322
0:16:35,74 --> 0:16:43,08
And when you reach 16 sub transactions
on a replica, so and you

323
0:16:43,08 --> 0:16:46,64
have a long-running transaction,
differently, some select for

324
0:16:46,64 --> 0:16:50,14
example, or just somebody open
a transaction and sitting, not consuming

325
0:16:50,14 --> 0:16:51,02
XID anymore.

326
0:16:51,28 --> 0:16:57,26
On the primary, on all replicas,
at some point you will see huge

327
0:16:57,26 --> 0:17:1,74
degradation and some sub-trans
SLRU contention.

328
0:17:2,38 --> 0:17:3,36
Lightweight lock.

329
0:17:4,6 --> 0:17:8,8
So it looks like a huge spike in
pg_stat_activity of active sessions

330
0:17:8,94 --> 0:17:12,38
and these active sessions are sitting
in wait event sub-trans

331
0:17:12,5 --> 0:17:13,0
SLRU.

332
0:17:13,94 --> 0:17:17,32
Of course, it happens again under
heavy load, like thousands

333
0:17:17,32 --> 0:17:17,98
of TPS.

334
0:17:18,16 --> 0:17:20,82
So you won't see it if you don't
have load enough.

335
0:17:21,24 --> 0:17:23,4
But it's a very hard problem to
overcome.

336
0:17:23,4 --> 0:17:27,52
So it's like, and also worth to
mention, it's great that you

337
0:17:27,52 --> 0:17:29,84
can see it in the wait events,
uptrans SLRU.

338
0:17:30,54 --> 0:17:36,06
Also very great that Amazon RDS
guys documented wait events.

339
0:17:36,06 --> 0:17:39,22
They have the best wait event documentation.

340
0:17:40,44 --> 0:17:45,32
Postgres itself doesn't have this
level of detail for wait events,

341
0:17:45,38 --> 0:17:48,0
so I'm constantly learning from
those documents.

342
0:17:48,34 --> 0:17:55,94
And also, if you have Postgres
13 or newer, there is a new system,

343
0:17:56,6 --> 0:17:57,1
pg_stat_slru.

344
0:17:59,12 --> 0:18:2,52
When we saw that at GitLab, unfortunately,
it was an older version.

345
0:18:2,72 --> 0:18:8,6
I was thinking, oh, so sad I'm
not having this view because it

346
0:18:8,6 --> 0:18:12,1
provides a lot of good counters
and you can see, okay, events

347
0:18:12,1 --> 0:18:14,94
of trans-SLRU is popping up there.

348
0:18:15,94 --> 0:18:17,58
This helps with diagnostics.

349
0:18:18,54 --> 0:18:24,36
And we also had, after spending
some hours, we replicated this

350
0:18:24,36 --> 0:18:28,04
problem in a synthetic environment
and discussed it.

351
0:18:28,12 --> 0:18:30,56
And the conclusion was we need
to eliminate sub-transactions.

352
0:18:31,08 --> 0:18:33,46
It was, again, as I said, a big
effort.

353
0:18:34,12 --> 0:18:35,86
Michael: I found that really interesting,
by the way.

354
0:18:35,86 --> 0:18:39,6
I found it really interesting why
reducing sub-transactions wasn't

355
0:18:39,6 --> 0:18:43,64
enough, and it was really a case
of actually eliminating them.

356
0:18:44,44 --> 0:18:44,94
Nikolay: Yeah.

357
0:18:45,06 --> 0:18:47,22
Well, sorry, I said 16, 32.

358
0:18:47,48 --> 0:18:54,52
Here, like 64 is for this local
cache and here 32, this limit

359
0:18:54,68 --> 0:18:58,6
of SLRU buffer size.

360
0:18:58,94 --> 0:19:4,74
So yeah, you can reduce them, but
it will happen only to some

361
0:19:4,74 --> 0:19:5,2
extent.

362
0:19:5,2 --> 0:19:11,04
The problem is this long-lasting
read-only transaction on the

363
0:19:11,04 --> 0:19:11,54
primary.

364
0:19:11,88 --> 0:19:17,2
It can be like you have this and
also the level of XID consumption.

365
0:19:17,52 --> 0:19:17,78
Yeah.

366
0:19:17,78 --> 0:19:18,14
Also.

367
0:19:18,14 --> 0:19:18,64
Right.

368
0:19:18,7 --> 0:19:25,8
And if you have high TPS, the higher
TPS you have, the shorter

369
0:19:25,8 --> 0:19:31,0
this long read-only transaction
needs to be so you can hit, or

370
0:19:31,0 --> 0:19:33,84
your replicas hit this performance
cliff.

371
0:19:34,04 --> 0:19:39,0
So, for example, if you have lower
TPS, you can still have this

372
0:19:39,0 --> 0:19:41,72
problem, but you will need to wait
more with this longer running

373
0:19:41,72 --> 0:19:42,22
transaction.

374
0:19:42,26 --> 0:19:43,28
So, let's also tell them

375
0:19:43,28 --> 0:19:46,16
Michael: you hit it if like the
queries, let's say, yeah, dozens

376
0:19:46,16 --> 0:19:48,5
of seconds or minutes or maybe
even longer.

377
0:19:48,62 --> 0:19:52,26
Whereas when I think you showed
nicely in your blog post or maybe

378
0:19:52,26 --> 0:19:57,8
it was the GitLab 1, if you ramp
that up enough, this could even

379
0:19:57,8 --> 0:19:59,44
be achieved within seconds.

380
0:19:59,44 --> 0:20:3,08
Like I think the synthetic 1 you
were benchmarking was like 18

381
0:20:3,08 --> 0:20:5,58
seconds or something, but it was
quite easy to see that this

382
0:20:5,58 --> 0:20:8,38
could be low numbers of seconds
as well, quite easily.

383
0:20:8,56 --> 0:20:11,26
Nikolay: Yeah, this, like we thought
about like, maybe we should

384
0:20:11,26 --> 0:20:16,26
just control long-running transactions
on primary and be more

385
0:20:16,26 --> 0:20:21,74
like proactive for beating them,
but 60, like seconds, right?

386
0:20:21,74 --> 0:20:22,44
It's like,

387
0:20:22,44 --> 0:20:23,5
Michael: ready to show.

388
0:20:24,14 --> 0:20:26,64
The reason I wanted to hammer that
home is I don't think most

389
0:20:26,64 --> 0:20:29,64
people think of two-second queries
when they're talking about

390
0:20:29,64 --> 0:20:30,72
long-running transactions.

391
0:20:31,4 --> 0:20:32,68
Nikolay: Yeah, long is relative.

392
0:20:33,26 --> 0:20:34,08
Yeah, true.

393
0:20:34,16 --> 0:20:38,82
And so, yeah, so also if you don't
use replicas for read-only

394
0:20:38,86 --> 0:20:40,6
traffic, there is no problem here.

395
0:20:40,6 --> 0:20:42,1
So it doesn't happen with primary.

396
0:20:42,4 --> 0:20:48,42
It was a difficulty when we needed
to troubleshoot it because

397
0:20:48,74 --> 0:20:51,98
with single-node experiment you
cannot reproduce it.

398
0:20:51,98 --> 0:20:55,78
You need two nodes, you need a replica
to see how it works.

399
0:20:56,04 --> 0:21:1,44
But again, we have publicly available
reproduction in a synthetic

400
0:21:1,48 --> 0:21:5,58
environment so you can play yourself
and see how it hits you.

401
0:21:6.1 --> 0:21:9.56
There are good attempts to improve
it.

402
0:21:9.72 --> 0:21:11.7
Also, Andrei Borodin is involved.

403
0:21:12.44 --> 0:21:13.72
This guy is everywhere.

404
0:21:14.54 --> 0:21:18.56
When I have some problem, I see
some effort already, he participates.

405
0:21:19.06 --> 0:21:24.26
So there is an effort to increase
the shared buffer size and

406
0:21:24.26 --> 0:21:28.84
also change algorithm, search algorithm
there and so on.

407
0:21:28.84 --> 0:21:29.54
It's interesting.

408
0:21:29.54 --> 0:21:32.66
I hope eventually it will make
it into source code.

409
0:21:32.66 --> 0:21:35.52
And finally, let's probably talk
where...

410
0:21:36.82 --> 0:21:40.16
My approach is let's eliminate
sub-transactions altogether if

411
0:21:40.24 --> 0:21:46.1
you aim to grow to many thousand
TPS and so on.

412
0:21:46.3 --> 0:21:51.1
But I think we need to eliminate
it in source code of application

413
0:21:53.2 --> 0:21:55.3
because we have less control on
it.

414
0:21:55.44 --> 0:22:0.28
But sometimes I still use sub-transactions,
for example, when

415
0:22:0.28 --> 0:22:2.68
deploying some complex DDL.

416
0:22:4.4 --> 0:22:9.82
Because when you need to do something,
and then you need to perform

417
0:22:9.96 --> 0:22:14.62
some additional step, and this
step might fail because you are

418
0:22:14.62 --> 0:22:14.87
gentle.

419
0:22:14.87 --> 0:22:21.3
I mean, you know you need to acquire
a lock, but you have a low

420
0:22:21.76 --> 0:22:23.24
lock timeout and retries.

421
0:22:24.6 --> 0:22:29.8
You can do this retry at a higher
level, but you will need to redo

422
0:22:29.8 --> 0:22:32.12
all the steps before it, right?

423
0:22:32.52 --> 0:22:32.86
Michael: Or you

424
0:22:32.86 --> 0:22:35.88
Nikolay: can do a retry here with
sub-transactions.

425
0:22:38.3 --> 0:22:40.64
Michael: And crucially, you want
a transaction; like the transaction's

426
0:22:40.76 --> 0:22:43.98
giving you a lot of benefits that
it will roll back if it fails

427
0:22:44.06 --> 0:22:44.56
ultimately.

428
0:22:45.36 --> 0:22:49.24
So the transaction itself, the
top-level transaction is really

429
0:22:49.24 --> 0:22:49.74
valuable.

430
0:22:49.84 --> 0:22:53.94
So you can't get rid of that.

431
0:22:54.64 --> 0:22:55.84
You can't do it in steps.

432
0:22:55.84 --> 0:22:57.54
You need to do it all or nothing.

433
0:22:58.26 --> 0:22:58.62
Nikolay: Right.

434
0:22:58.62 --> 0:23:1.88
For example, you have some table,
you put some data into it and

435
0:23:1.88 --> 0:23:4.4
then you establish foreign key
and you need to establish a foreign

436
0:23:4.4 --> 0:23:5.84
key and you need to retry some
stuff.

437
0:23:5.84 --> 0:23:10.12
But we also need to make sure these
retries don't last long if

438
0:23:10.12 --> 0:23:14.92
you don't want to keep all the
locks you already acquired so

439
0:23:14.92 --> 0:23:21.72
far because locks, the rule, remember,
the rule, locks are being

440
0:23:21.72 --> 0:23:24.66
held until the very end of the
transaction, rollback or commit.

441
0:23:24.66 --> 0:23:28.78
They cannot be freed in the middle
of the transaction, right?

442
0:23:28.78 --> 0:23:32.78
This is impossible, so you should
be careful with all locks already

443
0:23:32.88 --> 0:23:33.38
acquired.

444
0:23:34.02 --> 0:23:38.3
So yeah, here we can use certain
actions, retry logic, and then

445
0:23:38.3 --> 0:23:45.26
you just deploy it when we have
a lower level of TPS and we definitely

446
0:23:45.28 --> 0:23:48.76
don't have long transactions, read-only
transactions, or other

447
0:23:48.76 --> 0:23:50.18
transactions on the primary.

448
0:23:50.38 --> 0:23:50.88
Michael: Great.

449
0:23:50.9 --> 0:23:51.88
Nikolay: This is the key.

450
0:23:51.98 --> 0:23:52.48
Yeah.

451
0:23:53,48 --> 0:23:53,98
Yeah.

452
0:23:56,82 --> 0:23:59,48
So it's an interesting story, 2
years old already.

453
0:24:0,04 --> 0:24:3,3
But it was super interesting to
see these things.

454
0:24:3,82 --> 0:24:10,58
And my last words, I'm very thankful
that it's implemented in

455
0:24:10,58 --> 0:24:12,9
Postgres and I hope it will be
improved.

456
0:24:12,9 --> 0:24:16,72
I mean, I'm not an enemy of sub-transactions
like some people

457
0:24:16,72 --> 0:24:17,46
might think.

458
0:24:17,9 --> 0:24:20,76
I think it's a great feature, but
we should be very careful under

459
0:24:20,76 --> 0:24:21,52
heavy loads.

460
0:24:23,0 --> 0:24:24,48
Michael: Yeah, it makes a lot of
sense.

461
0:24:24,82 --> 0:24:28,5
There's a couple more super minor
things that I wanted to mention

462
0:24:28,5 --> 0:24:31,04
before we finish, and that's the
sub-trans SLRU.

463
0:24:33,58 --> 0:24:35,64
It's in Postgres 12 and older.

464
0:24:35,64 --> 0:24:39,44
I know Postgres 12 is the only
older version that is still supported.

465
0:24:39,6 --> 0:24:42,18
But it had a different name, right?

466
0:24:42,18 --> 0:24:43,68
Subtransactions Control Lock.

467
0:24:44,24 --> 0:24:45,18
Just in case.

468
0:24:45,24 --> 0:24:45,74
Right.

469
0:24:47,24 --> 0:24:47,74
Yeah.

470
0:24:48,22 --> 0:24:53,34
And the last 1 was the GitLab blog
post ended on really positive

471
0:24:53,4 --> 0:24:58,28
note that the reason they were
able to diagnose this with you

472
0:24:59,24 --> 0:25:3,3
with some help from other people
internally was that Postgres's

473
0:25:3,82 --> 0:25:8,16
source code is well documented
and that the source code's open

474
0:25:8,16 --> 0:25:12,46
and this would have been a really
difficult issue to diagnose

475
0:25:12,72 --> 0:25:17,12
on a closed-source system or anyway
I thought that was a really

476
0:25:17,12 --> 0:25:21,64
nice point that even though Postgres
does have this issue or

477
0:25:22,08 --> 0:25:25,86
can hit this issue at scale using
save points or subtransactions

478
0:25:26,12 --> 0:25:29,94
in general, the fact that it's
open gives it that huge benefit

479
0:25:30,04 --> 0:25:32,42
of people being able to diagnose
the issue themselves.

480
0:25:34,54 --> 0:25:35,72
Nikolay: Right, makes sense.

481
0:25:38,12 --> 0:25:45,04
So I hope it's also just like,
just keep in mind, that's it.

482
0:25:46,56 --> 0:25:51,48
Use other people's issues already
documented to improve.

483
0:25:52,54 --> 0:25:53,04
Yeah.

484
0:25:53,8 --> 0:25:54,56
Michael: Nice 1.

485
0:25:54,76 --> 0:25:55,7
Well, thanks everybody.

486
0:25:55,76 --> 0:25:56,6
Thank you, Nikolay.

487
0:25:56,77 --> 0:25:57,94
Thank you.

488
0:25:58,08 --> 0:25:58,9
And happy Thanksgiving.

489
0:25:59,08 --> 0:26:0,2
Hope you enjoy it.

490
0:26:0,42 --> 0:26:1,92
And see you next week.