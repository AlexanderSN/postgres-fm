1
0:0:6,5 --> 0:0:9,4
Hello, welcome to PostgresFM episode
number 51.

2
0:0:9,96 --> 0:0:13,059999
And today I'm alone and first time
I do it online.

3
0:0:13,219999 --> 0:0:16,58
I mean, I did many things online
on PostgresTV.

4
0:0:17,4 --> 0:0:21,779999
If you follow just Postgres FM
podcast, I encourage you checking

5
0:0:21,779999 --> 0:0:24,439999
out YouTube channel, PostgresTV
as well.

6
0:0:25,08 --> 0:0:30,52
Since we publish their podcast
episodes regularly, like weekly,

7
0:0:31,26 --> 0:0:33,58
And this is actually week number
51.

8
0:0:33,58 --> 0:0:34,3
It's insane.

9
0:0:34,3 --> 0:0:36,0
Next week we will have anniversary.

10
0:0:37,04 --> 0:0:39,22
But also we have other sessions.

11
0:0:39,239998 --> 0:0:43,28
Sometimes we develop something
or we invite some guests and so

12
0:0:43,28 --> 0:0:43,78
on.

13
0:0:44,3 --> 0:0:47,14
But Well, 51 is insane.

14
0:0:47,16 --> 0:0:53,4
It feels insane to have each week
1 episode, and this is a great

15
0:0:53,4 --> 0:0:53,9
place.

16
0:0:54,0 --> 0:0:57,88
But unfortunately, my co-host Michael
is right now on vacation,

17
0:0:57,88 --> 0:1:2,92
so it's very hard for me to choose
a topic, to coordinate everything.

18
0:1:3,28 --> 0:1:8,68
For me, it's easier to do it online
because I don't need to redo

19
0:1:8,76 --> 0:1:9,86
it and so on.

20
0:1:10,28 --> 0:1:17,78
But I apologize in advance for
some, some and sounds because

21
0:1:17,92 --> 0:1:20,92
usually Michael edits it very well.

22
0:1:20,94 --> 0:1:25,16
I don't know how it will sound
on podcast, but I will do my best.

23
0:1:25,6 --> 0:1:30,24
So choosing topic today was hard,
as I said, but I went to Hacker

24
0:1:30,24 --> 0:1:30,74
News.

25
0:1:30,84 --> 0:1:33,54
I'm using usually Algolia search.

26
0:1:34,6 --> 0:1:36,42
I will provide the link below.

27
0:1:36,6 --> 0:1:42,24
So I'm using Algolia search and
I check just Postgres or PostgresQL.

28
0:1:42,479996 --> 0:1:47,32
Unfortunately, this search considers
these words not synonyms.

29
0:1:48,34 --> 0:1:52,12
And this week was actually hot
in terms of Polsgis topics on

30
0:1:52,12 --> 0:1:52,98
Hacker News.

31
0:1:54,02 --> 0:2:3,04
Many topics made it on the front
page, but The most popular 1

32
0:2:3,04 --> 0:2:5,96
was about threads versus processes.

33
0:2:7,34 --> 0:2:11,54
Maybe you know Heikki after PGCon
started this discussion in

34
0:2:11,54 --> 0:2:17,46
PGSQL hackers mailing list and
This topic became very popular,

35
0:2:18,06 --> 0:2:22,86
more than 100 emails in the mailing
list itself, and on Hacker

36
0:2:22,86 --> 0:2:27,18
News more than 300, I think 400
maybe, comments.

37
0:2:27,7 --> 0:2:33,16
Of course levels of discussion,
deepness or how to say, the depth

38
0:2:33,16 --> 0:2:34,94
of discussions are very different.

39
0:2:35,82 --> 0:2:40,24
But anyway, interesting to hear
a lot of opinions, but I'm not

40
0:2:40,24 --> 0:2:43,4
going to discuss it today, maybe
another time.

41
0:2:43,78 --> 0:2:51,38
And Next, number 2 topic was about
upgrades, major upgrades for

42
0:2:51,38 --> 0:2:55,62
Postgres using logical replication,
involving logical replication

43
0:2:55,64 --> 0:2:59,14
to achieve 0 or near to 0 downtime.

44
0:3:0,06 --> 0:3:4,24
This topic is very interesting
and I have a lot of things to

45
0:3:4,24 --> 0:3:8,56
say and to share, but maybe later
because there is some ongoing

46
0:3:8,56 --> 0:3:10,32
work which is not done yet.

47
0:3:10,84 --> 0:3:15,8
Maybe later this year we will share
something which will be very

48
0:3:15,8 --> 0:3:16,3
interesting.

49
0:3:17,52 --> 0:3:18,96
So I chose the third topic.

50
0:3:18,96 --> 0:3:24,2
The third topic was related to
a blog post on the Cybertech blog

51
0:3:24,52 --> 0:3:28,02
by Ans Asma, and it was about UUID.

52
0:3:28,94 --> 0:3:36,5
It's a quite well-known problem
with traditional UUID, and it's

53
0:3:36,5 --> 0:3:37,48
related to performance.

54
0:3:37,96 --> 0:3:45,26
But this post was, as usual in
cybertech blog, it was quite concrete

55
0:3:45,26 --> 0:3:50,52
in terms of practical examples
and numbers and so on.

56
0:3:50,66 --> 0:3:52,04
So it's worth discussing.

57
0:3:52,9 --> 0:3:58,22
But let me start from some history
and from some distance.

58
0:3:59,54 --> 0:4:5,5
When I co-founded my first social
network and chose Postgres

59
0:4:5,5 --> 0:4:6,6
for the first time.

60
0:4:6,6 --> 0:4:11,04
I started it on using MySQL, but
it took me just 1 week to realize

61
0:4:11,04 --> 0:4:14,7
that I'm not going to stay on MySQL
because it was a terrible

62
0:4:14,7 --> 0:4:15,2
time.

63
0:4:15,36 --> 0:4:21,44
When MySQL was version 3 and so
on and so on, not following what

64
0:4:21,44 --> 0:4:23,36
we learned at the university.

65
0:4:23,5 --> 0:4:27,04
So I quickly chose Postgres and
converted very quickly.

66
0:4:27,72 --> 0:4:33,3
And we had interesting, it was
2005 or so, very long ago.

67
0:4:33,9 --> 0:4:38,7
And we decided that we need to
hide real numbers from our competitors.

68
0:4:40,08 --> 0:4:45,48
And we wanted to, you know, like
when you decide to use surrogate

69
0:4:45,54 --> 0:4:54,26
key, you probably see, you probably
let your users see current

70
0:4:54,26 --> 0:4:58,2
number, for example, you assign
it sequentially using...

71
0:4:58,74 --> 0:5:0,9
Back that time, we didn't have
generators.

72
0:5:1,08 --> 0:5:6,88
We had only serial or big serial
data types, sequences, right?

73
0:5:7,78 --> 0:5:14,34
And any user when user is registered
is assigned, we have a number

74
0:5:14,38 --> 0:5:18,3
assigned and user can guess how
many other users already registered.

75
0:5:18,44 --> 0:5:22,36
And if this is your competitor,
they probably will start checking

76
0:5:22,36 --> 0:5:23,34
you every week.

77
0:5:23,72 --> 0:5:29,98
And they will see the growth, like
rates of your growth, and

78
0:5:30,06 --> 0:5:34,3
not only about users, but also
some blog posts, comments, everything.

79
0:5:34,9 --> 0:5:37,08
We decided that we want to hide
it.

80
0:5:37,2 --> 0:5:38,92
An interesting trick was chosen.

81
0:5:38,92 --> 0:5:43,14
It was not my idea, but I implemented
it quickly using simple

82
0:5:43,14 --> 0:5:44,32
defaults and sequences.

83
0:5:44,72 --> 0:5:53,4
So what we did, we wanted some
random numbers, but not real random

84
0:5:53,4 --> 0:5:53,8
numbers.

85
0:5:53,8 --> 0:5:58,58
So if you take 2 mutually prime
numbers and just multiply sequence

86
0:5:58,58 --> 0:6:5,9
value by 1 of them, and then use
model operation with another

87
0:6:5,9 --> 0:6:14,32
number, you will have like looking
like kind of random numbers,

88
0:6:14,54 --> 0:6:18,42
but it will not be really random
numbers and it will be extremely

89
0:6:18,48 --> 0:6:23,34
hard even if probably not possible
to understand, not knowing

90
0:6:23,34 --> 0:6:27,88
these 2 mutually prime numbers
to understand what is happening.

91
0:6:27,88 --> 0:6:29,86
So it will look like some random
numbers.

92
0:6:29,86 --> 0:6:33,04
Of course 1 of these 2 mutually
prime numbers should be very

93
0:6:33,04 --> 0:6:34,84
large, very big number.

94
0:6:34,84 --> 0:6:42,04
So you have like ring or circle
and you're walking on this circle.

95
0:6:42,44 --> 0:6:45,14
So what we did, it was great.

96
0:6:45,28 --> 0:6:46,38
It was working great.

97
0:6:46,38 --> 0:6:52,26
We used this approach for all our
surrogate primary keys, and

98
0:6:52,68 --> 0:6:53,36
that's it.

99
0:6:53,36 --> 0:6:59,28
So it was a single primary, then
we dealt with various scalability

100
0:6:59,44 --> 0:7:2,38
issues using Slony, then Londesta,
and so on.

101
0:7:2,5 --> 0:7:7,98
But in the second social network
I co-founded a few years later,

102
0:7:8,56 --> 0:7:12,18
we used the same approach, and
then we ended up having a real

103
0:7:12,18 --> 0:7:13,94
nasty problem for the first time.

104
0:7:13,94 --> 0:7:16,74
It was 2008, I remember it.

105
0:7:16,84 --> 0:7:22,0
And we solved the problem when
you reach 2.1 billion records,

106
0:7:22,5 --> 0:7:27,1
if you use integer 4 primary key,
you don't...

107
0:7:28,28 --> 0:7:33,62
In some point, when your table
is growing very fast, 2.1 billion

108
0:7:33,62 --> 0:7:34,12
rows.

109
0:7:34,46 --> 0:7:37,62
I mean, not rows, but value 2.1
billion.

110
0:7:37,86 --> 0:7:41,02
You cannot insert, Postgres cannot
insert anymore, and you have

111
0:7:43,44 --> 0:7:45,36
very big problem to solve.

112
0:7:45,9 --> 0:7:50,14
If it's, To solve it synchronously,
you need to have some downtime,

113
0:7:50,34 --> 0:7:51,96
many hours of downtime, unfortunately.

114
0:7:52,42 --> 0:7:58,18
So since then I learned you never
should use integer-4 primary

115
0:7:58,18 --> 0:7:58,68
keys.

116
0:7:58,94 --> 0:8:4,42
But What's interesting here, we
used basically some kind of random

117
0:8:5,28 --> 0:8:6,64
distribution for numbers.

118
0:8:7,42 --> 0:8:11,28
And at that time, we didn't realize
that it's not good in terms

119
0:8:11,28 --> 0:8:11,92
of performance.

120
0:8:12,34 --> 0:8:13,94
Why it's not good in terms of performance?

121
0:8:13,94 --> 0:8:18,66
Because if you, Usually in social
network, social media, most

122
0:8:18,66 --> 0:8:24,56
of the time you select users, ask
you to select from your database

123
0:8:25,24 --> 0:8:30,12
most recent data.

124
0:8:30,56 --> 0:8:35,5
For example, provide the last 25
comments or 100 comments for

125
0:8:35,5 --> 0:8:41,7
this post, or show most fresh,
interesting posts and so on.

126
0:8:42,7 --> 0:8:47,8
And if you use kind of random numbers
or looking like random

127
0:8:47,8 --> 0:8:54,24
but not real random numbers, you
end up using different pages

128
0:8:54,24 --> 0:8:56,78
for each insert in terms of heap.

129
0:8:56,8 --> 0:9:0,48
But not only in terms of heap,
but also for indexes, Because

130
0:9:0,48 --> 0:9:4,92
if you use index, probably you
use by default, it's B-tree, you

131
0:9:4,92 --> 0:9:8,86
end up inserting in different subtrees
in B-tree.

132
0:9:9,62 --> 0:9:12,38
So sometimes it's good, but not
always.

133
0:9:12,4 --> 0:9:19,56
If you have just single node And
your usage pattern is like fresh

134
0:9:20,28 --> 0:9:22,62
data is needed more often than
old data.

135
0:9:23,04 --> 0:9:24,74
In this case, it's not optimal.

136
0:9:26,32 --> 0:9:28,78
And we didn't realize that at that
time.

137
0:9:28,78 --> 0:9:32,94
And then we also saw our competitors
don't care anymore about

138
0:9:33,82 --> 0:9:35,46
hiding some numbers and so on.

139
0:9:35,46 --> 0:9:37,72
And at some point I decided also
not...

140
0:9:37,74 --> 0:9:41,68
And my third social network, and
since then I don't care about

141
0:9:41,68 --> 0:9:46,58
this, like, masking real numbers
and I just use...

142
0:9:46,58 --> 0:9:51,08
I used integer 8 primary keys and
all good.

143
0:9:51,72 --> 0:9:58,58
But now we live in the world where
we use many, many, many nodes.

144
0:10:0,04 --> 0:10:5,38
For example, just yesterday I needed
to compare behavior of 2

145
0:10:5,38 --> 0:10:11,24
major Postgres versions, and I
needed to dump some samples and

146
0:10:11,24 --> 0:10:15,56
plans and so on, and then I needed
to analyze plans' behavior,

147
0:10:16,22 --> 0:10:21,36
explain analyze buffers on 2 nodes,
and then to compare.

148
0:10:21,54 --> 0:10:27,94
And obviously, if I used just regular
integer 8 and sequential

149
0:10:28,04 --> 0:10:32,66
assigned, I would have collisions
when merging data from 2 nodes.

150
0:10:32,66 --> 0:10:38,14
Because, for example, ID 1 is used
on both servers, so we cannot

151
0:10:38,14 --> 0:10:38,94
merge this data.

152
0:10:38,94 --> 0:10:42,32
And it's like, it's solvable, but
it's not interesting.

153
0:10:42,38 --> 0:10:43,7
So I just used UUID.

154
0:10:44,76 --> 0:10:49,24
You install extension and you start
using UUID and you know collisions

155
0:10:49,24 --> 0:10:52,62
are extremely rare, so it's all
good.

156
0:10:53,26 --> 0:10:59,44
But regular approach, when you,
so-called traditional UUID, so-called

157
0:10:59,58 --> 0:11:6,64
version 8, sorry, version 4, it
behaves like randomly distributed.

158
0:11:7,46 --> 0:11:12,94
In this case, you know that collisions
are very unlikely.

159
0:11:13,38 --> 0:11:17,38
It's good to use it as a global
value.

160
0:11:17,92 --> 0:11:23,1
But since it's randomly distributed,
it's going to have not good

161
0:11:23,94 --> 0:11:28,04
the state of B3, because when you
need again, like you need to

162
0:11:28,78 --> 0:11:35,1
check your latest results, you
will use a lot of buffers.

163
0:11:35,6 --> 0:11:41,9
And this particular post in CyberTech
blog discusses this problem.

164
0:11:42,18 --> 0:11:45,82
But it goes into an interesting
direction which probably was

165
0:11:45,82 --> 0:11:48,42
not explored by others before.

166
0:11:48,7 --> 0:11:54,18
It discusses the performance of
aggregates of count and index-only

167
0:11:54,2 --> 0:11:54,7
scans.

168
0:11:55,24 --> 0:12:1,24
So with index-only scans we utilize
visibility maps and of course

169
0:12:1,24 --> 0:12:6,8
in this post the author performed
a vacuum analyze and so visibility

170
0:12:6,94 --> 0:12:11,78
map was fresh, no heap fetches
involved, only work with index.

171
0:12:12,08 --> 0:12:15,96
So select count, blah blah blah,
and you check some fresh, count

172
0:12:15,96 --> 0:12:16,185
of fresh records.

173
0:12:16,185 --> 0:12:17,06
Fresh, a count of fresh records.

174
0:12:17,66 --> 0:12:20,28
And the offer compared 3 cases.

175
0:12:20,9 --> 0:12:26,02
Integer 8, UUID version 4, traditional
UUID most people use.

176
0:12:26,64 --> 0:12:31,48
If in Postgres, this is what we
will use if you decide to use

177
0:12:31,48 --> 0:12:36,44
UUID, provided by Postgres itself,
by extension basically.

178
0:12:37,96 --> 0:12:44,4
And third option was a new kind
of UUID, so-called version 7,

179
0:12:44,64 --> 0:12:49,44
which is not standard yet, but
it is a work in progress standard

180
0:12:49,94 --> 0:12:51,1
reviewed right now.

181
0:12:52,2 --> 0:12:55,94
And this UID version 7 has interesting
property.

182
0:12:56,48 --> 0:13:3,22
Collisions are also extremely unlikely,
but values are generated

183
0:13:3,96 --> 0:13:8,8
in a kind of sequential like order.

184
0:13:8,8 --> 0:13:13,58
So if we order values generated,
so basically timestamp is involved

185
0:13:13,94 --> 0:13:14,94
under the hood.

186
0:13:15,16 --> 0:13:21,06
And if you generate values, you
can order by them and it will

187
0:13:21,06 --> 0:13:22,68
have correlation with time.

188
0:13:23,4 --> 0:13:29,18
So it keeps this good property
to be, like, you can use it globally,

189
0:13:29,28 --> 0:13:34,36
you can have many, many primary
servers generating this value,

190
0:13:34,6 --> 0:13:40,28
collisions almost in your life
will never happen, but you can

191
0:13:40,28 --> 0:13:45,22
order by this value and it will
be the same if you would order

192
0:13:45,52 --> 0:13:49,2
by created at colon or any colon.

193
0:13:49,94 --> 0:13:55,28
So similar to regular surrogate
integer 8 primary keys.

194
0:13:55,84 --> 0:14:1,1
And interesting result was that
if you create indexes, of course,

195
0:14:1,1 --> 0:14:7,12
bit indexes for all 3 options,
And then you, first of all, if

196
0:14:7,12 --> 0:14:11,06
you check, like, first thing interesting
was, like, I didn't

197
0:14:11,06 --> 0:14:12,94
pay attention to it, but it's interesting.

198
0:14:13,48 --> 0:14:18,7
Insert time itself already, where
version 4 traditionally UID

199
0:14:18,7 --> 0:14:19,4
is losing.

200
0:14:19,66 --> 0:14:20,32
It's interesting.

201
0:14:20,32 --> 0:14:24,36
It's not losing too much, but it's
already losing in terms of

202
0:14:24,36 --> 0:14:25,22
insert time.

203
0:14:28,36 --> 0:14:32,44
And then if you check select count,
This is the most interesting

204
0:14:32,44 --> 0:14:32,94
part.

205
0:14:32,98 --> 0:14:39,82
So timing for version 4 was much
worse, but the state was warmed,

206
0:14:40,24 --> 0:14:43,22
caches were warmed, so everything
was buffer hit.

207
0:14:43,78 --> 0:14:48,48
And there, then the author checks,
explain plans to understand

208
0:14:48,48 --> 0:14:50,86
why it's slower, why version 4
is slower.

209
0:14:51,02 --> 0:14:55,82
As for integer 8 and UUID version
7, quite close.

210
0:14:56,0 --> 0:14:56,76
Why so?

211
0:14:57,44 --> 0:15:0,92
And there I expected to see it
And I saw it.

212
0:15:0,92 --> 0:15:6,92
I saw buffer's option used in explain-analyze-plan
analysis.

213
0:15:9,0 --> 0:15:14,12
We discussed it many, many times,
and I will repeat it many more

214
0:15:14,12 --> 0:15:14,62
times.

215
0:15:14,82 --> 0:15:17,74
Buffers should be always used when
we analyze plans.

216
0:15:17,86 --> 0:15:21,3
It should be default, but unfortunately,
there were 2 attempts

217
0:15:21,44 --> 0:15:26,74
to make it default during the last
few years, but unfortunately,

218
0:15:26,82 --> 0:15:31,16
both attempts failed and both hackers
decided not to continue.

219
0:15:33,34 --> 0:15:37,06
I think everyone agrees it should
be default, but somehow it's

220
0:15:37,06 --> 0:15:40,46
related to test, test, Postgres,
automated test Postgres has

221
0:15:40,46 --> 0:15:41,12
and so on.

222
0:15:41,12 --> 0:15:42,24
It's hard to implement.

223
0:15:42,94 --> 0:15:48,96
But I think if someone wants this
goal to achieve, it's a great

224
0:15:48,96 --> 0:15:53,16
goal to make buffers option default
in explain analyze.

225
0:15:54,06 --> 0:16:0,98
So when we check buffers, surprise,
we see that for integer 4,

226
0:16:1,16 --> 0:16:6,76
for, for integer 8 and UID version
7, it's almost the same in

227
0:16:6,76 --> 0:16:7,66
terms of buffers.

228
0:16:7,72 --> 0:16:10,46
This explains similar timing.

229
0:16:11,18 --> 0:16:17,86
But for UUID version 4, it's almost
2 orders of magnitude more

230
0:16:18,08 --> 0:16:18,98
buffer hits.

231
0:16:19,64 --> 0:16:23,68
And then Ofler explains why it's
related to visibility maps.

232
0:16:24,44 --> 0:16:30,42
The need for Postgres to check
many more pages, many more buffers,

233
0:16:30,72 --> 0:16:35,42
because we don't have data locality
anymore inside visibility

234
0:16:35,74 --> 0:16:36,5
maps as well.

235
0:16:36,5 --> 0:16:39,32
And we keep checking many, many,
many buffers.

236
0:16:40,08 --> 0:16:41,5
So this is super interesting.

237
0:16:41,98 --> 0:16:49,2
And it means not only for fetching
fresh data, sequential generation

238
0:16:49,5 --> 0:16:57,16
of surrogate keys, either integers
or complex values like UID

239
0:16:57,38 --> 0:16:58,0
is better.

240
0:16:58,0 --> 0:17:2,42
But also for aggregates and counting,
you know, Postgres has

241
0:17:2,42 --> 0:17:3,16
slow count.

242
0:17:3,16 --> 0:17:4,54
It's always a problem.

243
0:17:4,54 --> 0:17:5,94
Like many people complain.

244
0:17:6,18 --> 0:17:10,7
It's, it's, it was improved with,
improvement of index only scans.

245
0:17:10,84 --> 0:17:14,58
If you have auto, auto vacuum tuned,
index only scans perform

246
0:17:14,58 --> 0:17:18,9
quite well and counting is faster,
but still, this is interesting

247
0:17:18,9 --> 0:17:19,4
example.

248
0:17:19,4 --> 0:17:27,02
So the bad recipe, forget about
what vacuum tuning and use UUID

249
0:17:27,1 --> 0:17:27,98
version 4.

250
0:17:28,18 --> 0:17:31,16
And you will end up having very
slow count.

251
0:17:31,96 --> 0:17:38,04
So, but good advice is use, don't
use version 4 UUID, use version

252
0:17:38,04 --> 0:17:43,1
7, which is not like official yet,
but you can write simple function

253
0:17:43,1 --> 0:17:43,62
to that.

254
0:17:43,62 --> 0:17:48,76
It's not a deal, you can find many
examples in internet, or you

255
0:17:48,76 --> 0:17:51,42
can even generate it on the application
side.

256
0:17:52,04 --> 0:17:56,1
And then also keep your visibility
maps up to date.

257
0:17:56,16 --> 0:17:59,24
For that, you need to tune out
the vacuum, because the vacuum

258
0:17:59,24 --> 0:18:0,66
updates visibility maps.

259
0:18:0,92 --> 0:18:6,84
Visibility map remembers which
page has all visible tuples.

260
0:18:7,26 --> 0:18:13,3
So in page we have many tuples
and all visible page means that

261
0:18:13,62 --> 0:18:17,28
we don't have transactions which
would need some tuple which

262
0:18:17,28 --> 0:18:18,22
probably is dead, right?

263
0:18:18,22 --> 0:18:21,24
So all tuples are visible to all
transactions.

264
0:18:21,96 --> 0:18:27,84
So it means like if, if you make
some recent delete or update,

265
0:18:28,66 --> 0:18:33,52
you produce some dead tuple, then
auto-vacuum needs to, to clean

266
0:18:33,52 --> 0:18:38,3
it, but also visibility, in visibility
map, the all visible flag

267
0:18:38,3 --> 0:18:41,92
will be set to false, meaning that
in this page, we probably

268
0:18:41,92 --> 0:18:46,0
have some tuples which are visible
to some transactions, not

269
0:18:46,0 --> 0:18:50,74
visible to some other transactions,
but if you keep auto vacuum

270
0:18:50,74 --> 0:18:56,7
settings quite aggressive, auto
vacuum is processing tables often.

271
0:18:57,1 --> 0:19:2,16
In this case, you will see a few
heap fetches in the plan for

272
0:19:2,16 --> 0:19:4,62
index-only scan and count will
be faster.

273
0:19:4,92 --> 0:19:8,0
But also you need the sequential
ID allocation.

274
0:19:8,5 --> 0:19:13,4
And so UUID version 7 is looking
very good.

275
0:19:14,38 --> 0:19:17,92
Funny thing that a few months ago
we on Postgres TV, we had a

276
0:19:17,92 --> 0:19:21,26
session with Andrei Borodin and
Kirk Volokh.

277
0:19:22,2 --> 0:19:27,26
And it was a Kirk's idea to let's
work on implementation of first,

278
0:19:27,26 --> 0:19:32,3
it was called UUID, But then we
learned that there is standard

279
0:19:32,78 --> 0:19:37,54
proposal, which describes UUID
version 7.

280
0:19:37,8 --> 0:19:41,18
So Andrei implemented it and sent
to hackers list.

281
0:19:41,18 --> 0:19:45,3
You can find it on Postgres TV,
YouTube channel.

282
0:19:45,78 --> 0:19:52,2
And We had good feedback that it's
indeed a feature which should

283
0:19:52,2 --> 0:19:53,0
be in Postgres.

284
0:19:53,76 --> 0:19:59,24
But Peter Eisentraut noted that
the standard is not finalized,

285
0:19:59,26 --> 0:20:0,9
it's not approved yet.

286
0:20:1,5 --> 0:20:4,48
Before approval, it's not good
to implement it.

287
0:20:4,48 --> 0:20:5,96
We should wait until approval.

288
0:20:6,22 --> 0:20:10,84
And then 1 of the authors of the
new version of standard popped

289
0:20:10,84 --> 0:20:15,32
up in the hackers mailing list,
responding that it's great that

290
0:20:15,32 --> 0:20:18,78
you do it and update it with some
details.

291
0:20:19,02 --> 0:20:20,72
So it was good work.

292
0:20:20,98 --> 0:20:27,0
Anyway, I've checked the state
of the standard and on June 10,

293
0:20:29,18 --> 0:20:34,04
this proposal changed its state
and now it's waiting for a new

294
0:20:34,04 --> 0:20:34,54
review.

295
0:20:35,08 --> 0:20:40,22
And it looks like probably later
this year, this standard update,

296
0:20:40,84 --> 0:20:46,3
standard change, RFC, I don't remember
the number, can be approved.

297
0:20:46,52 --> 0:20:51,72
And if it's approved, I think it
will be not a big deal to finalize

298
0:20:51,72 --> 0:20:58,62
the patch and in this case it will
probably make it way to Postgres

299
0:20:58,62 --> 0:20:59,12
17.

300
0:20:59,18 --> 0:21:1,44
If not, Okay, Postgres 18.

301
0:21:1,68 --> 0:21:3,7
But it's not a huge patch, right?

302
0:21:3,7 --> 0:21:8,76
So once the standard is approved,
I hope Postgres will have it.

303
0:21:9,44 --> 0:21:10,82
That's probably it.

304
0:21:11,76 --> 0:21:13,68
Again, it was hard for me to be
alone.

305
0:21:13,68 --> 0:21:16,54
I hope next week I won't be alone.

306
0:21:17,36 --> 0:21:19,94
If I will, it will be anniversary.

307
0:21:19,94 --> 0:21:20,78
It's interesting.

308
0:21:20,86 --> 0:21:22,42
We should do something special.

309
0:21:22,42 --> 0:21:26,58
If you have some questions, probably
we should have some session

310
0:21:26,58 --> 0:21:30,68
of answer Q&A, like ask me anything
session.

311
0:21:30,92 --> 0:21:36,16
So send me anything to Twitter,
to YouTube, anywhere, and we

312
0:21:36,16 --> 0:21:38,66
will probably work on those questions.

313
0:21:40,16 --> 0:21:44,76
And in 2 weeks, Michael will return
and it will be a normal podcast.

314
0:21:44,92 --> 0:21:49,54
Sorry if I was not like, I was
doing a lot of it's because no

315
0:21:49,54 --> 0:21:51,06
editing this time.

316
0:21:51,48 --> 0:21:53,04
Okay, thank you so much.

317
0:21:53,94 --> 0:22:0,94
Please share this episode in your
social networks and working

318
0:22:0,94 --> 0:22:5,8
groups and so on and give let me
let me know what you want next
